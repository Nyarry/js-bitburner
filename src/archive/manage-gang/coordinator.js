import {getMembers} from '/shared/aggregators/members'
import {CONSTANTS} from '/shared/constants'

let members = []

export async function main(ns) {
	await initialise(ns)

	while (true) {
		await recruit(ns)

		members = await getMembers(ns)

		await ascend(ns)
		await upgrade(ns)
		await assign(ns)

		await ns.sleep(2000)
	}
}

async function initialise(ns) {
	ns.disableLog('ALL')
	ns.tprint(`INFO: [manage-gang]: Initialising...`)
	await ns.sleep(500)

	if (!ns.gang.inGang()) {
		ns.tprint(`ERROR: [manage-gang]: You need to be in a gang. Terminating.`)
		ns.exit()
		return
	}

	ns.tprint(`INFO: [manage-gang]: Coordinator has been initialised.`)
}

async function recruit(ns) {
	if (!ns.gang.canRecruitMember()) return

	const names = [
		'Wrokod', 'Nurkrord', 'Vribogar', 'Mokgogas',
		'Churkag', 'Ragecrusha', 'Doomhacka', 'Facebrakkah',
		'Bigtrasha', 'Scalpcleava', 'Krirgnor', 'Mutogar',
		'Kruzdrugar', 'Kratzmam', 'Zargram', 'Jowlbrakkah',
		'Bludslashah', 'Dreamkrakah', 'Jowlgashah', 'Godhackah',
		'Kigbuz', 'Nahrblam', 'Adok', 'Wridozak', 'Zroghuz',
		'Rageboila', 'Guttrasha', 'Killcrashah', 'Hedcrackah', 'Crookwhacka'
	]

	for (const name of names) {
		const wasRecruited = ns.gang.recruitMember(name)
		if (!wasRecruited) continue

		ns.tprint(`INFO: [manage-gang]: Recruited member ${name} to the gang.`)
	}
}

async function ascend(ns) {
	for (const member of members) {
		if (member.ascensionResult) {
			const totalGain = member.ascensionResult.str + member.ascensionResult.def + member.ascensionResult.dex + member.ascensionResult.agi

			if (totalGain >= 12 && member.memberInfo.str >= 150) {
				await ns.gang.ascendMember(member.memberName)
				ns.tprint(`INFO: ${member.memberName} has ascended.`)
			}
		}
	}
}

async function upgrade(ns) {
	const gang = ns.gang.getGangInformation()
	const money = ns.getPlayer().money
	let upgrades = []

	for (const member of members) {
		if (member.buyableEquipment.length > 0) {
			upgrades.push(member)
		}
	}

	if (upgrades.length > 0) {
		upgrades = upgrades.sort((a, b) => {
			return ns.gang.getEquipmentCost(b.buyableEquipment[0]) - ns.gang.getEquipmentCost(a.buyableEquipment[0])
		})
	}

	for (const upgrade of upgrades) {
		const equipmentToBuy = upgrade.buyableEquipment[0]
		const cost = ns.gang.getEquipmentCost(equipmentToBuy)

		if ((money * 0.25) >= cost) {
			const wasPurchased = await ns.gang.purchaseEquipment(upgrade.memberName, equipmentToBuy)
			if (!wasPurchased) continue
			ns.tprint(`INFO: [manage-gang]: ${upgrade.memberName} has been equipped with [${equipmentToBuy}] for ${ns.nFormat(cost, '$0.0a')}.`)
		}
	}
}

async function getWarDecision(ns) {
	const gang = ns.gang.getGangInformation()
	const otherGangs = ns.gang.getOtherGangInformation()

	let rivals = []

	for (const otherGang of Object.entries(otherGangs)) {
		let rival = {faction: otherGang[0], power: otherGang[1].power, territory: otherGang[1].territory, clash: ns.gang.getChanceToWinClash(otherGang[0])}
		if (rival.faction === gang.faction) continue
		rivals.push(rival)
	}

	rivals = rivals.sort((a, b) => {
		return a.clash - b.clash
	})

	return rivals[0].clash > 0.75
}

async function assign(ns) {
	const gang = ns.gang.getGangInformation()
	const shouldWar = await getWarDecision(ns)

	for (const member of members) {
		if (member.memberInfo.str < 500) {
			ns.gang.setMemberTask(member.memberName, CONSTANTS.GANG_TASKS.TRAIN_COMBAT)
			continue
		}
		if (members.length < 12 || member.memberInfo.earnedRespect < 1e6) {
			ns.gang.setMemberTask(member.memberName, CONSTANTS.GANG_TASKS.TERRORISM)
			continue
		}
		if (member.memberInfo.str < 5000) {
			ns.gang.setMemberTask(member.memberName, CONSTANTS.GANG_TASKS.TRAIN_COMBAT)
			continue
		}
		if (!shouldWar && member.memberInfo.str >= 5000) {
			ns.gang.setMemberTask(member.memberName, CONSTANTS.GANG_TASKS.TERRITORY_WARFARE)
			continue
		}

		ns.gang.setMemberTask(member.memberName, CONSTANTS.GANG_TASKS.HUMAN_TRAFFICKING)
	}

	ns.gang.setTerritoryWarfare(shouldWar)
}