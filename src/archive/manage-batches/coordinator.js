import {CONSTANTS} from '/shared/constants'
import {secondsToString} from '/shared/functions'
import {getMachines} from '/shared/aggregators/machines'
import {AsciiTable} from '/thirdparty/ascii-table'

let machines = {}
let metrics = {}
let shouldRecalibrate = false
let table = new AsciiTable('manage-batches')

const scripts = [CONSTANTS.SCRIPTS.HACK, CONSTANTS.SCRIPTS.GROW, CONSTANTS.SCRIPTS.WEAKEN]
const startTime = Date.now()

const fractionOfMoneyToTake = 0.50
const delay = 100

export async function main(ns) {
	await initialise(ns)

	while (true) {
		machines = await getMachines(ns)

		await deployScriptsToWorkers(ns)
		await updateMetrics(ns)
		await prepareBatches(ns)
		await ns.sleep(5)
	}
}

async function initialise(ns) {
	ns.disableLog('ALL')
	ns.tprint(`INFO: [manage-batches]: Initialising...`)
	await ns.sleep(500)

	if (!ns.fileExists('Formulas.exe')) {
		ns.tprint(`ERROR: [manage-batches]: Requires Formulas.exe to run. Terminating.`)
		ns.exit()
	}

	ns.tprint(`INFO: [manage-batches]: Coordinator has been initialised.`)
}

async function deployScriptsToWorkers(ns) {
	for (const worker of machines.workers) {
		for (const script of scripts) {
			await ns.scp(`${script}`, worker.hostName)
		}
	}
}

async function getCurrentWorker(ns) {
	for (const worker of machines.workers) {
		if (worker.hostName === ns.getHostname()) {
			return worker
		}
	}
}

async function getTarget(ns) {
	for (const target of machines.targets) {
		if (target.primed) {
			return target
		}
	}
}

async function getHackThreads(ns, target, fractionOfMoneyToTake) {
	const hasFormulas = ns.fileExists('Formulas.exe')

	let numberOfThreads = 0
	let timeToHack = 0
	let fractionOfMoneyStolen = 0

	if (hasFormulas) {
		const server = ns.getServer(target.hostName)
		const player = ns.getPlayer()

		numberOfThreads = Math.floor(fractionOfMoneyToTake / ns.formulas.hacking.hackPercent(server, player))
		timeToHack = ns.formulas.hacking.hackTime(server, player)
		fractionOfMoneyStolen = ns.formulas.hacking.hackPercent(server, player)
	} else {
		numberOfThreads = Math.max(1, Math.floor(ns.hackAnalyzeThreads(target.hostName, fractionOfMoneyToTake)))
		timeToHack = ns.getHackTime(target.hostName)
		fractionOfMoneyStolen = ns.hackAnalyze(target.hostName)
	}

	const securityIncrease = ns.hackAnalyzeSecurity(numberOfThreads)
	const moneyToTake = fractionOfMoneyStolen * numberOfThreads * target.moneyMax

	return {numberOfThreads, timeToHack, securityIncrease, moneyToTake}
}

async function getGrowThreads(ns, target, moneyToTake) {
	const hasFormulas = ns.fileExists('Formulas.exe')
	const amountToRegrow = target.moneyMax / (target.moneyMax - Math.max(moneyToTake, 1))

	let numberOfThreads = 0
	let timeToGrow = 0

	if (hasFormulas) {
		const server = ns.getServer(target.hostName)
		const player = ns.getPlayer()

		numberOfThreads = Math.ceil(Math.log(amountToRegrow) / Math.log(ns.formulas.hacking.growPercent(server, 1, player)))
		timeToGrow = ns.formulas.hacking.growTime(server, player)
	} else {
		numberOfThreads = ns.growthAnalyze(target.hostName, amountToRegrow)
		timeToGrow = ns.getGrowTime(target.hostName)
	}

	const securityIncrease = ns.growthAnalyzeSecurity(numberOfThreads)

	return {numberOfThreads, timeToGrow, securityIncrease}
}

async function getWeakenThreads(ns, target, securityIncrease) {
	const hasFormulas = ns.fileExists('Formulas.exe')

	let numberOfThreads = Math.ceil(securityIncrease / ns.weakenAnalyze(1)) + 1
	let timeToWeaken = 0

	if (hasFormulas) {
		const server = ns.getServer(target.hostName)
		const player = ns.getPlayer()

		timeToWeaken = ns.formulas.hacking.weakenTime(server, player)
	} else {
		timeToWeaken = ns.getWeakenTime(target.hostName)
	}

	return {numberOfThreads, timeToWeaken}
}

async function getThreadsToRun(ns, target, fractionOfMoneyToTake) {
	const hackThreads = await getHackThreads(ns, target, fractionOfMoneyToTake)
	const weakenThreads1 = await getWeakenThreads(ns, target, hackThreads.securityIncrease)
	const growThreads = await getGrowThreads(ns, target, hackThreads.moneyToTake)
	const weakenThreads2 = await getWeakenThreads(ns, target, growThreads.securityIncrease)

	const threads = {
		h: hackThreads.numberOfThreads,
		w1: weakenThreads1.numberOfThreads,
		g: growThreads.numberOfThreads,
		w2: weakenThreads2.numberOfThreads
	}

	const timings = {
		h: hackThreads.timeToHack,
		w1: weakenThreads1.timeToWeaken,
		g: growThreads.timeToGrow,
		w2: weakenThreads2.timeToWeaken
	}

	return {threads, timings, moneyToTake: hackThreads.moneyToTake}
}

async function getDelays(delay, threadsToRun) {
	const delays = {
		h: threadsToRun.timings.w1 - delay - threadsToRun.timings.h,
		w1: 0,
		g: threadsToRun.timings.w1 + delay - threadsToRun.timings.g,
		w2: delay * 2
	}

	return delays
}

async function getBatches(ns, delay, threadsToRun, worker, target) {
	const delays = await getDelays(delay, threadsToRun)
	const firstHackArrival = delays.h + threadsToRun.timings.h

	const scriptRAM = {
		HACK: ns.getScriptRam(CONSTANTS.SCRIPTS.HACK),
		GROW: ns.getScriptRam(CONSTANTS.SCRIPTS.GROW),
		WEAKEN: ns.getScriptRam(CONSTANTS.SCRIPTS.WEAKEN),
	}

	const batchRAMCost = (threadsToRun.threads.h * scriptRAM.HACK) + (threadsToRun.threads.w1 * scriptRAM.WEAKEN) + (threadsToRun.threads.g * scriptRAM.GROW) + (threadsToRun.threads.w2 * scriptRAM.WEAKEN)
	const batchesToFit = Math.floor(worker.freeRAM / batchRAMCost)

	let batches = []
	let i = 0

	while (batchesToFit > i && firstHackArrival > delays.w1 + delay * (4 * i)) {
		batches.push({script: CONSTANTS.SCRIPTS.HACK, numberOfThreads: threadsToRun.threads.h, requiredRAM: threadsToRun.threads.h * scriptRAM.HACK, startTime: delays.h + delay * (4 * i)})
		batches.push({script: CONSTANTS.SCRIPTS.WEAKEN, numberOfThreads: threadsToRun.threads.w1, requiredRAM: threadsToRun.threads.w1 * scriptRAM.HACK, startTime: delays.w1 + delay * (4 * i)})
		batches.push({script: CONSTANTS.SCRIPTS.GROW, numberOfThreads: threadsToRun.threads.g, requiredRAM: threadsToRun.threads.g * scriptRAM.HACK, startTime: delays.g + delay * (4 * i)})
		batches.push({script: CONSTANTS.SCRIPTS.WEAKEN, numberOfThreads: threadsToRun.threads.w2, requiredRAM: threadsToRun.threads.w2 * scriptRAM.HACK, startTime: delays.w2 + delay * (4 * i)})

		i ++
	}

	batches = batches.sort((a, b) => {
		return a.startTime - b.startTime
	})

	if (batches.length > 0) {
		ns.tprint(`INFO: Prepared ${batchesToFit} batches targeting [${target.hostName}] from [${worker.hostName}].`)
		ns.tprint(`INFO: Finished in: ${secondsToString(batches[batches.length - 1].startTime / 1000)} for ${ns.nFormat(threadsToRun.moneyToTake * batchesToFit, '$0.0a')}`)
	}

	return batches
}

async function prepareBatches(ns) {
	const worker = await getCurrentWorker(ns)
	const target = await getTarget(ns)

	if (!target || !worker) return

	let threadsToRun = await getThreadsToRun(ns, target, fractionOfMoneyToTake)

	if (shouldRecalibrate) {
		// ns.tprint(`INFO: Waiting ${threadsToRun.timings.w1 + (delay * 2)} for scripts to terminate.`)
		// await ns.sleep(threadsToRun.timings.w1 + (delay * 2))

		ns.tprint(`INFO: Recalibrating. Terminating and restarting scripts.`)
		ns.killall(worker.hostName)
		ns.exec('/manage-overview/inject.js', 'home')
		ns.exec('/manage-primer/coordinator.js', 'home')
		ns.exec('/manage-batches/coordinator.js', 'home')

		threadsToRun = await getThreadsToRun(ns, target, fractionOfMoneyToTake)
		shouldRecalibrate = false
	}

	const batches = await getBatches(ns, delay, threadsToRun, worker, target)
	if (!batches) return

	shouldRecalibrate = await distributeBatches(ns, target, worker, batches)
}

async function distributeBatches(ns, target, worker, batches) {
	const initialHackingLevel = ns.getHackingLevel()
	let slept = 0

	for (const batch of batches) {
		const hackingLevel = ns.getHackingLevel()

		// if (hackingLevel !== initialHackingLevel) {
		// 	ns.tprint(`WARNING: Hacking level increased. Recalibrating.`)
		// 	return true
		// }

		if ((batch.startTime - slept) < 0) {
			ns.tprint(`WARNING: We've fallen behind. Recalibrating.`)
			return true
		}

		if (!target.primed) {
			ns.tprint(`WARNING: ${target.hostName} is no longer primed. Recalibrating.`)
			return true
		}

		await ns.sleep(batch.startTime - slept)
		slept = batch.startTime

		if (batch.requiredRAM > worker.freeRAM) {
			ns.tprint(`WARNING: Not enough RAM on ${worker.hostName}. Recalibrating.`)
			return true
		}

		if (ns.exec(batch.script, worker.hostName, batch.numberOfThreads, target.hostName, Date.now()) < 1) {
			ns.tprint(`WARNING: Unable to spawn script. Recalibrating.`)
			return true
		}
	}

	return false
}

async function updateMetrics(ns) {
	table.setHeading('hostName', 'weighting', 'security', '$', 'primed')

	for (let i = 1; i <= 7; i ++) {
		table.setAlign(i, AsciiTable.RIGHT)
	}

	table.clearRows()

	for (const target of machines.targets) {
		table.addRow(
			target.hostName,
			`${ns.nFormat(target.weighting, '0.000')}`,
			`${ns.nFormat(target.securityDifference, '0.0')}`,
			`${ns.nFormat(target.moneyRatio, '0.0%')}`,
			`${target.primed}`
		)
	}

	ns.print(table.toString())
}