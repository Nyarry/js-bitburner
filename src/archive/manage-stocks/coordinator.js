import {AsciiTable} from '/thirdparty/ascii-table'

let stocks = {}
let metrics = {}
let table = new AsciiTable('manage-stocks')

export async function main(ns) {
	await initialise(ns)

	while (true) {
		await updateStockInfo(ns)
		await updateMarketPositions(ns)
		await updateStockMetrics(ns)
		await ns.sleep(2000)
	}
}

async function initialise(ns) {
	ns.disableLog('ALL')
	ns.tprint(`INFO [manage-stocks]: Initialising...`)
	await ns.sleep(500)

	ns.tprint(`INFO [manage-stocks]: Coordinator has been initialised.`)
}

async function getStockSentiments(ns) {
	let [bullish, bearish] = [[], []]

	for (const stock of Object.values(stocks)) {
		if ((stock.forecast - stock.volatility) >= 0.55) {
			bullish.push(stock)
		}
		if ((stock.forecast - stock.volatility) <= 0.51) {
			bearish.push(stock)
		}
	}

	bullish = bullish.sort((a, b) => {
		return b.forecast - a.forecast
	})

	return {bullish, bearish}
}

async function updateStockInfo(ns) {
	const symbols = ns.stock.getSymbols()

	for (const symbol of symbols) {
		const position = ns.stock.getPosition(symbol)

		stocks[symbol] = {
			symbol: symbol,
			forecast: ns.stock.getForecast(symbol),
			volatility: ns.stock.getVolatility(symbol),
			askPrice: ns.stock.getAskPrice(symbol),
			bidPrice: ns.stock.getBidPrice(symbol),
			maxShares: ns.stock.getMaxShares(symbol),
			shares: position[0],
			sharesAveragePrice: position[1],
			sharesShort: position[2],
			sharesAveragePriceShort: position[3]
		}

		if (!metrics[symbol]) {
			metrics[symbol] = {symbol: symbol, profit: 0}
		}
	}
}

async function updateMarketPositions(ns) {
	const sentiments = await getStockSentiments(ns)

	for (const stock of sentiments.bearish) {
		if (stock.shares <= 0) continue

		const value = ns.stock.sellStock(stock.symbol, stock.shares)

		if (value === 0) continue

		const totalValue = stock.shares * value
		const totalCost = stock.shares * stock.sharesAveragePrice
		const profit = totalValue - totalCost

		if (profit > 0) {
			ns.toast(`Sold ${ns.nFormat(stock.shares, '0,0')} shares of ${stock.symbol} for a profit of ${ns.nFormat(profit, '$0.0a')}`, 'success', 3000)
		} else {
			ns.toast(`Sold ${ns.nFormat(stock.shares, '0,0')} shares of ${stock.symbol} at a loss of ${ns.nFormat(profit, '$0.0a')}`, 'error', 3000)
		}

		stocks[stock.symbol].shares = 0
		metrics[stock.symbol].profit += profit
	}

	for (const stock of sentiments.bullish) {
		const money = ns.getPlayer().money * 0.25
		const sharesToBuy = Math.min(money / stock.askPrice, stock.maxShares - stock.shares - stock.sharesShort)
		const costPerShare = ns.stock.buyStock(stock.symbol, sharesToBuy)

		if (sharesToBuy === 0 || costPerShare === 0) return

		stocks[stock.symbol].shares += sharesToBuy
	}
}

async function updateStockMetrics(ns) {
	table.setHeading('symbol', 'shares', 'sentiment', 'invested', 'value', 'profit')

	for (let i = 1; i <= 7; i ++) {
		table.setAlign(i, AsciiTable.RIGHT)
	}

	table.clearRows()

	for (const stock of Object.values(stocks)) {
		table.addRow(
			`${stock.symbol}`,
			`${ns.nFormat(stock.shares, '0,0')}`,
			`${ns.nFormat(stock.forecast - stock.volatility, '0.00')}`,
			`${ns.nFormat(stock.shares * stock.sharesAveragePrice, '$0.0a')}`,
			`${ns.nFormat(stock.shares * ns.stock.getAskPrice(stock.symbol), '$0.0a')}`,
			`${ns.nFormat(metrics[stock.symbol] ? metrics[stock.symbol].profit : 0, '$0.0a')}`
		)
	}

	ns.print(table.toString())
}
