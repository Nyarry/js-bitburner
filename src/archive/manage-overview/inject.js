export async function main(ns) {
	await initialise(ns)

	while (true) {
		await aggregateStats(ns)
		await ns.sleep(1000)
	}
}

async function initialise(ns) {
	ns.disableLog('ALL')
	ns.tprint(`INFO: [manage-overview]: Initialising...`)
	await ns.sleep(500) 

	ns.tprint(`INFO: [manage-overview]: Stats have been injected.`)
	ns.tprint('hello')
	ns.print('wtf????')
}

async function aggregateStats(ns) {
	const doc = eval(document)
	const hook0 = doc.getElementById('overview-extra-hook-0')
	const hook1 = doc.getElementById('overview-extra-hook-1')

	let [headers, values] = [[], []]

	headers, values = await addRatInfo(ns, headers, values)
	headers, values = await addBatchInfo(ns, headers, values)
	headers, values = await addStockInfo(ns, headers, values)
	headers, values = await addKarmaInfo(ns, headers, values)

	hook0.innerText = headers.join(' \n')
	hook1.innerText = values.join(' \n')

	ns.atExit(() => {
		hook0.innerHTML = ''
		hook1.innerHTML = ''
	})
}

async function addRatInfo(ns, headers, values) {
	const script = '/manage-rats/coordinator.js'
	const worker = 'home'

	if (!ns.scriptRunning(script, worker)) {
		return headers, values
	}

	headers.push('Rats')
	values.push(`${ns.nFormat(ns.getScriptIncome(script, worker), '$0.0a')} /s`)

	return headers, values
}

async function addBatchInfo(ns, headers, values) {
	const script = '/manage-batches/coordinator.js'
	const worker = 'home'

	if (!ns.scriptRunning(script, worker)) {
		return headers, values
	}

	headers.push('Batches')
	values.push(`${ns.nFormat(ns.getScriptIncome(script, worker), '$0.0a')} /s`)

	return headers, values
}

async function addStockInfo(ns, headers, values) {
	const script = '/manage-batches/coordinator.js'
	const worker = 'home'

	if (!ns.scriptRunning(script, worker)) {
		return headers, values
	}

	if (!ns.stock.hasWSEAccount) {
		ns.tprint(`WARNING: [manage-overview]: You need a WSE account to display stock info.`)
		return
	}

	let totalInvestedInStocks = 0

	const symbols = ns.stock.getSymbols()

	for (const symbol of symbols) {
		const position = ns.stock.getPosition(symbol)
		totalInvestedInStocks += position[0] * position[1]
	}

	headers.push(`Stocks`)
	values.push(`${ns.nFormat(totalInvestedInStocks, '$0.0a')}`)

	return headers, values
}

async function addKarmaInfo(ns, headers, values) {
	headers.push('Karma')
	values.push(`${ns.nFormat(ns.heart.break(), '0,0')}`)

	return headers, values
}