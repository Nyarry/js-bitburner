import {getNodes} from '/shared/aggregators/nodes'
import {CONSTANTS} from '/shared/constants'

export async function main(ns) {
	await initialise(ns)

	while (true) {
		// await upgrade(ns)
		await spend(ns)
		await ns.sleep(25)
	}
}

async function initialise(ns) {
	ns.disableLog('ALL')
	ns.tprint(`INFO [manage-nodes]: Initialising...`)
	await ns.sleep(500)

	ns.tprint(`INFO [manage-nodes]: Coordinator has been initialised.`)
}

async function upgrade(ns) {
	const nodes = await getNodes(ns)
	let candidate = null

	for (const node of nodes) {
		const gains = ns.hacknet.hashCapacity() > 0 ? node.gains.hashes : node.gains.money

		for (const [key, value] of Object.entries(gains)) {
			const prospect = {index: node.index, cost: node.costs[key], gain: gains[key], callback: node.callbacks[key]}

			if (!candidate) {
				candidate = prospect
			} else {
				if ((prospect.gain / prospect.cost) > (candidate.gain / candidate.cost)) {
					candidate = prospect
				}
			}
		}
	}

	if (candidate) {
		const money = ns.getPlayer().money

		if (money >= candidate.cost) {
			candidate.callback(candidate.index, 1)
			// ns.tprint(`Upgraded: ${JSON.stringify(candidate)}`)
		} else {
			// ns.tprint(`Candidate (can't afford): ${JSON.stringify(candidate)}`)
		}
	}
}

async function spend(ns) {
	if (ns.hacknet.hashCapacity() <= 0) return

	const upgrade = CONSTANTS.HACKNET.UPGRADES.SELL_FOR_MONEY

	while (ns.hacknet.numHashes() >= ns.hacknet.hashCost(upgrade, 1)) {
		ns.hacknet.spendHashes(upgrade, 1)
	}
}