const personalServerName = 'grogbim'

export async function main(ns) {
	await initialise(ns)

	while (true) {
		await purchaseMachines(ns)
		await upgradeMachines(ns)
		await ns.sleep(2000)
	}
}

async function initialise(ns) {
	ns.disableLog('ALL')
	ns.tprint(`INFO: [manage-servers]: Initialising...`)
	await ns.sleep(500)

	ns.tprint(`INFO: [manage-servers]: Coordinator has been initialised.`)
}

async function purchaseMachines(ns) {
	const servers = ns.getPurchasedServers()
	const limit = ns.getPurchasedServerLimit()

	if (servers.length >= limit) return

	const baseRAM = 8
	const cost = ns.getPurchasedServerCost(baseRAM)

	let iteration = 1
	let purchasedServers = []

	while (iteration <= limit) {
		const money = ns.getPlayer().money

		if (money > cost) {
			const serverName = `${personalServerName}.${iteration}`
			const doesServerExist = ns.serverExists(serverName)

			if (!doesServerExist) {
				purchasedServers.push({hostName: serverName, baseRAM, cost})
				ns.purchaseServer(serverName, baseRAM)
			}
		}

		++ iteration;
	}

	if (purchasedServers.length > 0) {
		const totalRAM = purchasedServers.reduce((total, server) => total + server.baseRAM, 0)
		const totalCost = purchasedServers.reduce((total, server) => total + server.cost, 0)
		const message = `Purchased ${purchasedServers.length} server${purchasedServers.length !== 1 ? 's' : ''} with ${ns.nFormat(totalRAM * 1e9, '0.0b')} RAM for ${ns.nFormat(totalCost, '$0.0a')}`

		ns.print(`INFO: ${message}`)
		ns.toast(message, 'success', 3000)
	}
}

async function upgradeMachines(ns) {
	const servers = ns.getPurchasedServers()
	const limit = ns.getPurchasedServerLimit()

	if (servers.length < limit) return

	let iteration = 1
	let upgradedServers = []

	while (iteration <= limit) {
		const server = servers[iteration - 1]
		const baseRAM = ns.getServerMaxRam(server)
		const upgradedRAM = baseRAM * 2
		const cost = ns.getPurchasedServerCost(upgradedRAM)
		const money = ns.getPlayer().money

		if ((money * 0.25) > cost) {
			await ns.killall(server)
			await ns.deleteServer(server)

			const machine = await ns.purchaseServer(server, upgradedRAM)
			upgradedServers.push({hostName: machine, baseRAM, upgradedRAM, cost})
		}

		++ iteration
	}

	if (upgradedServers.length > 0) {
		const totalBaseRAM = upgradedServers.reduce((total, server) => total + server.baseRAM, 0)
		const totalUpgradedRAM = upgradedServers.reduce((total, server) => total + server.upgradedRAM, 0)
		const totalCost = upgradedServers.reduce((total, server) => total + server.cost, 0)
		const message = `Upgraded ${upgradedServers.length} server${upgradedServers.length !== 1 ? 's' : ''} from ${ns.nFormat(totalBaseRAM * 1e9, '0.0b')} to ${ns.nFormat(totalUpgradedRAM * 1e9, '0.0b')} RAM for ${ns.nFormat(totalCost, '$0.0a')}`

		ns.print(`INFO: ${message}`)
		ns.toast(message, 'success', 3000)
	}
}