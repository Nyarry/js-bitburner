export async function main(ns) {
	const target = ns.args[0]
	const stolenMoney = await ns.hack(target)
	await ns.tryWritePort(1, JSON.stringify({hostName: target, stolenMoney: stolenMoney}))
}