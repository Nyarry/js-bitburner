const SCRIPT_HACK = '/shared/directives/hack.js'
const SCRIPT_GROW = '/shared/directives/grow.js'
const SCRIPT_WEAKEN = '/shared/directives/weaken.js'

export const CONSTANTS = {
	SCRIPTS: {
		HACK: SCRIPT_HACK,
		GROW: SCRIPT_GROW,
		WEAKEN: SCRIPT_WEAKEN
	},
	GANG_TASKS: {
		TRAIN_COMBAT: 'Train Combat',
		MUG_PEOPLE: 'Mug People',
		TERRORISM: 'Terrorism',
		HUMAN_TRAFFICKING: 'Human Trafficking',
		TERRITORY_WARFARE: 'Territory Warfare'
	},
	HACKNET: {
		MONEY_PER_LEVEL: 1.5,
		HASHES_PER_LEVEL: 0.001,
		UPGRADES: {
			SELL_FOR_MONEY: 'Sell for Money'
		}
	}
}