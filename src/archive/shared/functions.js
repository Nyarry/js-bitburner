export function secondsToString(seconds, isLongFormat) {
	const h = Math.floor(seconds / 3600)
	const m = Math.floor(seconds % 3600 / 60)
	const s = Math.floor(seconds % 3600 % 60)

	if (isLongFormat) {
		let strHours = h > 0 ? h + (h == 1 ? ' hour' : ' hours') : ''
		let strMinutes = m > 0 ? m + (m == 1 ? ' minute' : ' minutes') : ''
		let strSeconds = s > 0 ? s + (s == 1 ? ' second' : ' seconds') : ''

		if (strHours != '') {
			if (strMinutes != '' && strSeconds != '') {
				strHours += ', '
			} else if (strMinutes == '' && strSeconds != '') {
				strHours += ', and '
			}
		}
		if (strMinutes != '') {
			if (strSeconds != '') {
				strMinutes += ', and '
			}
		}

		return strHours + strMinutes + strSeconds
	} else {
		const strHours = h > 0 ? h + (h == 1 ? 'h ' : 'h ') : ''
		const strMinutes = m > 0 ? m + (m == 1 ? 'm ' : 'm ') : ''
		const strSeconds = s > 0 ? s + (s == 1 ? 's' : 's') : ''

		return strHours + strMinutes + strSeconds
	}
}