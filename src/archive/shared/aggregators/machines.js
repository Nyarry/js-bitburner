let machines = []

export async function getMachines(ns) {
	ns.disableLog('ALL')

	await ping(ns, ns.scan())
	await validate(ns)
	await amend(ns)

	const workers = await getWorkers(ns)
	const targets = await getTargets(ns)

	return {workers, targets}
}

async function ping(ns, network) {
	for (const hostName of network) {
		if (hostName === 'darkweb)') continue
		if (hostName.includes('hacknet-node')) continue

		const exists = machines.map(machine => machine.hostName).indexOf(hostName) >= 0 ? true : false
		if (exists) continue

		let machine = {
			hostName: hostName
		}

		machines.push(machine)

		let subnet = await ns.scan(hostName)

		if (subnet.length > 0) {
			await ping(ns, subnet)
		}
	}
}

async function validate(ns) {
	let validated = []
	let rooted = {hosts: [], totalRAM: 0, totalMoneyMax: 0}

	for (const machine of machines) {
		if (ns.serverExists(machine.hostName)) {
			const wasRooted = await siege(ns, machine.hostName)

			validated.push(machine)

			if (wasRooted) {
				rooted.hosts.push(wasRooted)
			}
		}
	}

	for (const host of rooted.hosts) {
		rooted.totalRAM += ns.getServerMaxRam(host)
		rooted.totalMoneyMax += ns.getServerMaxMoney(host)
	}

	if (rooted.hosts.length > 0) {
		const message = `Rooted ${rooted.hosts.length} machines with access to ${ns.nFormat(rooted.totalRAM * 1e9, '0.0b')} RAM, and ${ns.nFormat(rooted.totalMoneyMax, '$0.0a')} in max reserves.`
		ns.toast(message, 'success', 6000)
		ns.print(`INFO: ${message}`)
	}

	machines = validated
}

async function siege(ns, hostName) {
	if (!hostName) return
	if (ns.hasRootAccess(hostName)) return
	if (hostName === 'darkweb') return

	const programs = [
		{file: 'BruteSSH.exe', callback: ns.brutessh},
		{file: 'FTPCrack.exe', callback: ns.ftpcrack},
		{file: 'relaySMTP.exe', callback: ns.relaysmtp},
		{file: 'HTTPWorm.exe', callback: ns.httpworm},
		{file: 'SQLInject.exe', callback: ns.sqlinject}
	]

	let portsRequired = ns.getServerNumPortsRequired(hostName)
	let portsOpened = 0

	for (const program of programs) {
		if (ns.fileExists(program.file)) {
			program.callback(hostName)
			portsOpened ++
		}
	}

	if (portsOpened >= portsRequired) {
		ns.nuke(hostName)
		return hostName
	}

	return false
}

async function amend(ns) {
	let amended = []

	for (const machine of machines) {
		const server = ns.getServer(machine.hostName)
		const moneyMax = ns.getServerMaxMoney(machine.hostName)
		const moneyAvailable = ns.getServerMoneyAvailable(machine.hostName)
		const moneyRatio = moneyAvailable / moneyMax
		const securityMin = ns.getServerMinSecurityLevel(machine.hostName)
		const securityCurrent = ns.getServerSecurityLevel(machine.hostName)
		const securityDifference = securityCurrent - securityMin
		const maxRAM = ns.getServerMaxRam(machine.hostName)
		const usedRAM = ns.getServerUsedRam(machine.hostName)

		amended.push({
			hostName: machine.hostName,
			server: server,
			rooted: ns.hasRootAccess(machine.hostName),
			backdoored: server.backdoorInstalled,
			openPorts: server.openPortCount,
			requiredOpenPorts: server.numOpenPortsRequired,
			requiredHackingLevel: ns.getServerRequiredHackingLevel(machine.hostName),
			moneyMax: moneyMax,
			moneyAvailable: moneyAvailable,
			moneyRatio: moneyRatio,
			securityMin: securityMin,
			securityCurrent: securityCurrent,
			securityDifference: securityDifference,
			maxRAM: maxRAM,
			usedRAM: usedRAM,
			freeRAM: maxRAM - usedRAM,
			primed: securityDifference === 0 && moneyRatio === 1
		})
	}

	machines = amended
}

async function getWorkers(ns) {
	let workers = []

	for (const machine of machines) {
		if (!machine.rooted) continue
		workers.push(machine)
	}

	workers = workers.sort((a, b) => {
		return b.maxRAM - a.maxRAM
	})

	return workers
}

async function getTargets(ns) {
	let targets = []

	for (const machine of machines) {
		if (!machine.rooted) continue
		if (ns.getHackingLevel() < machine.requiredHackingLevel) continue
		if (machine.moneyMax <= 0) continue

		targets.push(machine)
	}

	for (const target of targets) {
		target.weighting = await getTargetWeighting(ns, target)
	}

	targets = targets.sort((a, b) => {
		return b.weighting - a.weighting
	})

	return targets
}

async function getTargetWeighting(ns, target) {
	const gainPerHack = (target.moneyAvailable * ns.hackAnalyze(target.hostName)) * ns.hackAnalyzeChance(target.hostName)
	const gainPerSecond = gainPerHack / ns.getHackTime(target.hostName)

	let threadWeighting = 1

	threadWeighting += Math.ceil(target.securityDifference / 0.05)
	threadWeighting += Math.ceil(ns.growthAnalyze(target.hostName, isFinite(1 / target.moneyRatio) ? 1 / target.moneyRatio : 1))

	return (gainPerSecond / threadWeighting) * 100
}