export async function getMembers(ns) {
	let members = []

	const gang = ns.gang.getGangInformation()
	const memberNames = ns.gang.getMemberNames()

	for (const memberName of memberNames) {
		const memberInfo = ns.gang.getMemberInformation(memberName)
		const ascensionResult = ns.gang.getAscensionResult(memberName)
		const ownedEquipment = memberInfo.upgrades.concat(memberInfo.augmentations)
		const equipmentNames = ns.gang.getEquipmentNames()

		let buyableEquipment = []

		for (const equipmentName of equipmentNames) {
			const upgradesToSkipIfCombat = ['NUKE Rootkit', 'Soulstealer Rootkit', 'Demon Rootkit', 'Hmap Node', 'Jack the Ripper']
			const augmentsToSkipIfCombat = ['BitWire', 'DataJack', 'Neuralstimulator']
			const upgradesToSkipIfHacking = ['Weapons', 'Armor', 'Vehicles']
			const augmentsToSkipIfHacking = ['Bionic Arms', 'Bionic Legs', 'Bionic Spine', 'BrachiBlades', 'Nanofiber Weave', 'Synthetic Heart', 'Synfibril Muscle', 'Graphene Bone Lacings']

			if (gang.isHacking) {
				if (upgradesToSkipIfHacking.includes(equipmentName)) continue
				if (augmentsToSkipIfHacking.includes(equipmentName)) continue
			} else {
				if (upgradesToSkipIfCombat.includes(equipmentName)) continue
				if (augmentsToSkipIfCombat.includes(equipmentName)) continue
			}

			if (!ownedEquipment.includes(equipmentName)) {
				buyableEquipment.push(equipmentName)
			}
		}

		buyableEquipment = buyableEquipment.sort((a, b) => {
			return ns.gang.getEquipmentCost(a) - ns.gang.getEquipmentCost(b)
		})

		const member = {
			memberName,
			memberInfo,
			ascensionResult,
			buyableEquipment
		}

		members.push(member)
	}

	return members
}