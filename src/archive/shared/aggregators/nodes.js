import {CONSTANTS} from '/shared/constants'

export async function getNodes(ns) {
	let nodes = []

	for (let i = 0; i < ns.hacknet.numNodes(); i ++) {
		const stats = ns.hacknet.getNodeStats(i)

		const costs = {
			level: ns.hacknet.getLevelUpgradeCost(i, 1),
			RAM: ns.hacknet.getRamUpgradeCost(i, 1),
			core: ns.hacknet.getCoreUpgradeCost(i, 1),
			cache: ns.hacknet.getCacheUpgradeCost(i, 1)
		}

		const gains = {
			money: {
				level: getMoneyGainRate(ns, stats.level + 1, stats.ram, stats.cores),
				RAM: getMoneyGainRate(ns, stats.level, getNextRAMIncrement(ns, stats.ram), stats.cores),
				core: getMoneyGainRate(ns, stats.level, stats.ram, stats.cores + 1)
			},
			hashes: {
				level: getHashGainRate(ns, stats.level + 1, stats.ram, stats.cores),
				RAM: getHashGainRate(ns, stats.level, getNextRAMIncrement(ns, stats.ram), stats.cores),
				core: getHashGainRate(ns, stats.level, stats.ram, stats.cores + 1)
			}
		}

		const callbacks = {
			level: ns.hacknet.upgradeLevel,
			RAM: ns.hacknet.upgradeRam,
			core: ns.hacknet.upgradeCore,
			cache: ns.hacknet.upgradeCache
		}

		const node = {
			index: i,
			stats: ns.hacknet.getNodeStats(i),
			costs,
			gains,
			callbacks
		}

		nodes.push(node)
	}

	return nodes
}

function getMoneyGainRate(ns, level, RAM, cores) {
	let levelMultiplier = CONSTANTS.HACKNET.MONEY_PER_LEVEL * level
	let RAMMultiplier = Math.pow(1.035, RAM - 1)
	let coreMultiplier = (cores + 5) / 6
	return levelMultiplier * RAMMultiplier * coreMultiplier
}

function getHashGainRate(ns, level, RAM, cores) {
	const baseGain = CONSTANTS.HACKNET.HASHES_PER_LEVEL * level
	const RAMMultiplier = Math.pow(1.07, Math.log2(RAM))
	const coreMultiplier = 1 + (cores - 1) / 5
	return baseGain * RAMMultiplier * coreMultiplier
}

function getNextRAMIncrement(ns, RAM) {
	let levelsToMax = Math.round(Math.log2(64 / RAM))
	let timesUpgraded = Math.log2(64) - levelsToMax
	return Math.pow(2, timesUpgraded + 1)
}