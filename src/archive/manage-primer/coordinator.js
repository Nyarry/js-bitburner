import {CONSTANTS} from '/shared/constants'
import {getMachines} from '/shared/aggregators/machines'
import {AsciiTable} from '/thirdparty/ascii-table'

let machines = {}
let state = {}
let metrics = {}
let table = new AsciiTable('manage-primer')

const scripts = [CONSTANTS.SCRIPTS.HACK, CONSTANTS.SCRIPTS.GROW, CONSTANTS.SCRIPTS.WEAKEN]
const startTime = Date.now()

export async function main(ns) {
	await initialise(ns)

	while (true) {
		machines = await getMachines(ns)

		await deployScriptsToWorkers(ns)
		await calculateThreads(ns)
		await allocateThreads(ns)

		while (ns.peek(1) !== 'NULL PORT DATA') {
			await amendStolenMoneyToMetrics(ns)
		}

		await updateTargetMetrics(ns)
		await ns.sleep(2000)
	}
}

async function initialise(ns) {
	ns.disableLog('ALL')
	ns.tprint(`INFO: [manage-primer]: Initialising...`)
	await ns.sleep(500)

	ns.tprint(`INFO: [manage-primer]: Coordinator has been initialised.`)
}

async function deployScriptsToWorkers(ns) {
	for (const worker of machines.workers) {
		for (const script of scripts) {
			await ns.scp(script, worker.hostName)
		}
	}
}

async function lookupTargetStateByScript(target, script) {
	switch (script) {
		case CONSTANTS.SCRIPTS.WEAKEN:
			return state[target.hostName].w
		case CONSTANTS.SCRIPTS.GROW:
			return state[target.hostName].g
		case CONSTANTS.SCRIPTS.HACK:
			return state[target.hostName].h
	}
}

async function calculateThreads(ns) {
	for (const target of machines.targets) {
		state[target.hostName] = {
			w: {threadsInUse: 0, requiredThreads: 0, totalThreads: 0},
			g: {threadsInUse: 0, requiredThreads: 0, totalThreads: 0},
			h: {threadsInUse: 0, requiredThreads: 0, totalThreads: 0}
		}

		for (const worker of machines.workers) {
			for (const script of scripts) {
				const runningScript = ns.getRunningScript(script, worker.hostName, target.hostName)
				if (!runningScript) continue
				let data = await lookupTargetStateByScript(target, script)
				data.threadsInUse = runningScript.threads
			}
		}

		if (target.securityDifference > 0) {
			const threadsToWeaken = Math.ceil(target.securityDifference / 0.05)
			state[target.hostName].w.requiredThreads = threadsToWeaken - state[target.hostName].w.threadsInUse
			state[target.hostName].w.totalThreads = threadsToWeaken
		}
		if (target.moneyRatio !== 1 && target.moneyRatio > 0) {
			const threadsToGrow = Math.ceil(ns.growthAnalyze(target.hostName, 1 / target.moneyRatio))
			state[target.hostName].g.requiredThreads = threadsToGrow - state[target.hostName].g.threadsInUse
			state[target.hostName].g.totalThreads = threadsToGrow
		}
	}
}

async function allocateThreads(ns) {
	for (const target of machines.targets) {
		for (const worker of machines.workers) {
			if (worker.hostName === 'home') continue

			for (const script of scripts) {
				let data = await lookupTargetStateByScript(target, script)

				const numberOfPotentialThreads = Math.floor((ns.getServerMaxRam(worker.hostName) - ns.getServerUsedRam(worker.hostName)) / ns.getScriptRam(script))
				const numberOfThreadsNeeded = data.requiredThreads - data.threadsInUse
				const numberOfThreadsToAllocate = Math.min(numberOfThreadsNeeded, numberOfPotentialThreads)

				if (data.requiredThreads > 0 && numberOfThreadsToAllocate > 0) {
					data.threadsInUse = Math.ceil(data.threadsInUse + numberOfThreadsToAllocate)
					ns.exec(script, worker.hostName, numberOfThreadsToAllocate, target.hostName)
				}
			}
		}
	}
}

async function amendStolenMoneyToMetrics(ns) {
	const data = JSON.parse(ns.readPort(1))

	if (!metrics[data.hostName]) {
		metrics[data.hostName] = data
	}

	metrics[data.hostName].stolenMoney += data.stolenMoney
}

async function updateTargetMetrics(ns) {
	let threadsInUse = 0
	let totalThreads = 0

	for (const target of machines.targets) {
		for (const script of scripts) {
			let data = await lookupTargetStateByScript(target, script)

			threadsInUse += data.threadsInUse
			totalThreads = data.totalThreads
		}
	}

	table.setHeading('hostName', 't=weaken', 't=grow', 't=hack', 'weighting', 'security', '$', 'stolen')

	for (let i = 1; i <= 7; i ++) {
		table.setAlign(i, AsciiTable.RIGHT)
	}

	table.clearRows()

	for (const target of machines.targets) {
		const w = state[target.hostName].w
		const g = state[target.hostName].g
		const h = state[target.hostName].h

		table.addRow(
			target.hostName,
			`${ns.nFormat(w.threadsInUse, '0')} (${ns.nFormat(w.totalThreads, '0')})`,
			`${ns.nFormat(g.threadsInUse, '0')} (${ns.nFormat(g.totalThreads, '0')})`,
			`${ns.nFormat(h.threadsInUse, '0')} (${ns.nFormat(h.totalThreads, '0')})`,
			`${ns.nFormat(target.weighting, '0.000')}`,
			`${ns.nFormat(target.securityDifference, '0.0')}`,
			`${ns.nFormat(target.moneyRatio, '0.0%')}`,
			`${ns.nFormat(metrics[target.hostName] ? metrics[target.hostName].stolenMoney : 0, '$0.0a')}`
		)
	}

	ns.print(table.toString())
}