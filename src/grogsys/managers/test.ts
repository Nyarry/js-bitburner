import {type NS} from '@ns'
import {initialise} from 'grogsys/shared/functions/initialise'
import {notify} from 'grogsys/shared/functions/notify'

export async function main(ns: NS): Promise<void> {
	await initialise(ns, 'Grogtest')

	notify(ns, 'terminal', 'info', 'Grogtest -> hello')
	//notify(ns, 'terminal', 'warning', 'Grogtest -> hello')
	//notify(ns, 'terminal', 'error', 'Grogtest -> hello')
	//notify(ns, 'terminal', 'success', 'Grogtest -> hello')

	//notify(ns, 'script', 'info', 'Grogtest -> hello')
	//notify(ns, 'script', 'warning', 'Grogtest -> hello')
	//notify(ns, 'script', 'error', 'Grogtest -> hello')
	//notify(ns, 'script', 'success', 'Grogtest -> hello')

	notify(ns, 'toast', 'info', 'Grogtest -> hello')
	//notify(ns, 'toast', 'warning', 'Grogtest -> hello')
	//notify(ns, 'toast', 'error', 'Grogtest -> hello')
	//notify(ns, 'toast', 'success', 'Grogtest -> hello')
}