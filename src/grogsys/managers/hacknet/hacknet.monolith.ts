import {type NS} from '@ns'
import type {Node, NodeProspect, NodeState, NodeGains} from 'grogsys/shared/types'
import {initialise} from 'grogsys/shared/functions/index'
import {getNodes} from 'grogsys/shared/aggregators/nodes'

let tick: number = 0

export async function main(ns: NS): Promise<void> {
	await initialise(ns, 'Hacknet.Monolith')
	
	while (true) {
		tick ++

		await upgradeNodes(ns, 10)
		//await spendHashesOnMoney(ns)
		//await spendHashesOnCorpFunds(ns)
		await ns.sleep(1000)
	}
}

async function upgradeNodes(ns: NS, timesToExecute: number): Promise<void> {
	for (let i = 0; i < timesToExecute; i ++) {
		const nodes: Node[] = await getNodes(ns)

		let nodeProspect: NodeProspect | undefined = undefined

		for (const node of nodes) {
			const state: NodeState = ns.hacknet.hashCapacity() > 0 ? node.gains.hashes : node.gains.money
			
			for (const [key, _] of Object.entries(state)) {
				const dynamicAccessor = key as keyof Omit<NodeState, 'cache'> 

				const specimen: NodeProspect = {
					index: node.index,
					upgradeType: dynamicAccessor,
					cost: node.costs[dynamicAccessor],
					gain: state[dynamicAccessor],
					callback: node.callbacks[dynamicAccessor]
				}

				if (!nodeProspect || ((specimen.gain / specimen.cost) > (nodeProspect.gain / nodeProspect.cost))) {
					nodeProspect = specimen
				}
			}
		}

		if (nodeProspect) {
			const money: number = ns.getPlayer().money

			if (money >= nodeProspect.cost) {
				nodeProspect.callback(nodeProspect.index, 1)

				const message: string = `Hacknet: Upgraded ${nodeProspect.upgradeType} of Node ${nodeProspect.index} for $${ns.formatNumber(nodeProspect.cost)}.`
				ns.print(message)
			}
		}
	}
}

async function spendHashesOnMoney(ns: NS) {
	if (tick % 50 !== 0 && ns.hacknet.numHashes() !== ns.hacknet.hashCapacity()) return
	if (ns.hacknet.hashCapacity() <= 0) return

	const hashes: number = ns.hacknet.numHashes()
	const hashCostPerPurchase: number = 4
	const moneyReceivedPerPurchase: number = 1000000
	const numberOfTimesToExchange: number = Math.floor(hashes / hashCostPerPurchase)
	const totalHashCost: number = ns.hacknet.hashCost('Sell for Money', numberOfTimesToExchange)
	const wasSuccessful: boolean = ns.hacknet.spendHashes('Sell for Money', undefined, numberOfTimesToExchange)

	if (wasSuccessful && numberOfTimesToExchange > 0) {
		const message: string = `Hacknet: Exchanged ${ns.formatNumber(totalHashCost)} hashes for $${ns.formatNumber(numberOfTimesToExchange * moneyReceivedPerPurchase)}`
		ns.print(message)
		ns.toast(message, 'success', 10000)
	}
}

async function spendHashesOnCorpFunds(ns: NS) {
	if (ns.hacknet.hashCapacity() <= 0) return

	const hashes: number = ns.hacknet.numHashes()
	const wasSuccessful: boolean = ns.hacknet.spendHashes('Sell for Corporation Funds', undefined, 1)

	if (wasSuccessful) {
		const message: string = `Hacknet: Invested $1b into Grogcorp.`
		ns.print(message)
		ns.toast(message, 'success', 10000)
	}
}