import {type NS} from '@ns'
import {type CorpConstants} from '@ns'
import {initialise} from 'grogsys/shared/functions/initialise'
import {manageSales} from './corporation.monolith'

export async function main(ns: NS): Promise<void> {
	await initialise(ns, 'Grogcorp.Sales')

	const constants: CorpConstants = ns.corporation.getConstants()

	while (true) {
		await manageSales(ns)
		await ns.sleep(constants.secondsPerMarketCycle)
	}
}