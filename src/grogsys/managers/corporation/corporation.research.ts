import {type NS} from '@ns'
import {type CorpConstants} from '@ns'
import {initialise} from 'grogsys/shared/functions/initialise'
import {manageResearch} from './corporation.monolith'

export async function main(ns: NS): Promise<void> {
	await initialise(ns, 'Grogcorp.Research')

	const constants: CorpConstants = ns.corporation.getConstants()

	while (true) {
		await manageResearch(ns)
		await ns.sleep(constants.secondsPerMarketCycle)
	}
}