import {type NS} from '@ns'
import {type CorpConstants} from '@ns'
import {initialise} from 'grogsys/shared/functions/initialise'
import {manageWarehouse} from './corporation.monolith'

export async function main(ns: NS): Promise<void> {
	await initialise(ns, 'Grogcorp.Warehouse')

	const constants: CorpConstants = ns.corporation.getConstants()

	while (true) {
		await manageWarehouse(ns)
		await ns.sleep(constants.secondsPerMarketCycle)
	}
}