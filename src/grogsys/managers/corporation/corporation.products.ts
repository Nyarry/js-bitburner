import {type NS} from '@ns'
import {type CorpConstants} from '@ns'
import {initialise} from 'grogsys/shared/functions/initialise'
import {manageProducts} from './corporation.monolith'

export async function main(ns: NS): Promise<void> {
	await initialise(ns, 'Grogcorp.Products')

	const constants: CorpConstants = ns.corporation.getConstants()

	while (true) {
		await manageProducts(ns)
		await ns.sleep(constants.secondsPerMarketCycle)
	}
}