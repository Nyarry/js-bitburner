import {type NS} from '@ns'
import {type CorpConstants} from '@ns'
import {initialise} from 'grogsys/shared/functions/initialise'
import {manageEmployees} from './corporation.monolith'

export async function main(ns: NS): Promise<void> {
	await initialise(ns, 'Grogcorp.Employees')

	const constants: CorpConstants = ns.corporation.getConstants()

	while (true) {
		await manageEmployees(ns)
		await ns.sleep(constants.secondsPerMarketCycle)
	}
}