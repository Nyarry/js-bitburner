import {type NS} from '@ns'
import type {Division, CityName, Office, CorporationInfo, CorpEmployeePosition, CorpIndustryData, CorpConstants, CorpMaterialName, Product, CorpUpgradeName, Warehouse, Material, CorpResearchName} from '@ns'
import type {PrioritisedResearch} from 'grogsys/shared/types'
import {initialise} from 'grogsys/shared/functions/initialise'
import {notify} from 'grogsys/shared/functions/notify'

let shouldUpgradeWarehouseSize: boolean = false

export async function main(ns: NS): Promise<void> {
	await initialise(ns, 'Grogcorp.Monolith')
	
	const constants: CorpConstants = ns.corporation.getConstants()

	shouldUpgradeWarehouseSize = await ns.prompt('Upgrade Warehouse size?', {type: 'boolean', choices: ['Yes', 'No']}) as boolean

	while (true) {
		await manageEmployees(ns)
		await manageResearch(ns)
		await manageUpgrades(ns, 1)
		await manageProducts(ns)
		await manageSales(ns)
		await manageWarehouse(ns)

		await ns.sleep(constants.secondsPerMarketCycle)
	}
}

async function hireEmployees(ns: NS): Promise<void> {
	const corp: CorporationInfo = ns.corporation.getCorporation()

	for (const divisionName of corp.divisions) {
		const division: Division = ns.corporation.getDivision(divisionName)
		const cityNames: CityName[] = division.cities

		for (const cityName of cityNames) {
			const office: Office = ns.corporation.getOffice(divisionName, cityName)
			if (office.numEmployees === office.size) continue

			const hirable: number = office.size - office.numEmployees

			for (let i = 0; i < hirable; i ++) {
				ns.corporation.hireEmployee(divisionName, cityName, 'Unassigned')
			}

			notify(ns, 'script', 'success', `Grogcorp: ${divisionName} -> ${cityName} -> Hired ${hirable} employees.`)
		}
	}
}

async function satisfyEmployees(ns: NS): Promise<void> {
	const corp: CorporationInfo = ns.corporation.getCorporation()

	for (const divisionName of corp.divisions) {
		const division: Division = ns.corporation.getDivision(divisionName)
		const cityNames: CityName[] = division.cities

		for (const cityName of cityNames) {
			const office: Office = ns.corporation.getOffice(divisionName, cityName)

			if (office.avgEnergy < office.maxEnergy) {
				const wasSuccessful: boolean = ns.corporation.buyTea(divisionName, cityName)

				if (wasSuccessful) {
					notify(ns, 'script', 'success', `Grogcorp: ${divisionName} -> ${cityName} -> Tea.`)
				}
			}
			
			if (office.avgMorale < office.maxMorale) {
				const wasSuccessful: number = ns.corporation.throwParty(divisionName, cityName, 1e6)

				if (wasSuccessful) {
					notify(ns, 'script', 'success', `Grogcorp: ${divisionName} -> ${cityName} -> Party.`)
				}
			}
		}
	}
}

async function unassignEmployees(ns: NS): Promise<void> {
	const corp: CorporationInfo = ns.corporation.getCorporation()
	const constants: CorpConstants = ns.corporation.getConstants()

	for (const divisionName of corp.divisions) {
		const division: Division = ns.corporation.getDivision(divisionName)
		const cityNames: CityName[] = division.cities

		for (const cityName of cityNames) {
			for (const job of constants.employeePositions) {
				ns.corporation.setAutoJobAssignment(divisionName, cityName, job, 0)
			}
		}
	}
}

async function hasAllResearchBeenCompleted(ns: NS, divisionName: string, cityName: string) {
	const division: Division = ns.corporation.getDivision(divisionName)
	const constants: CorpConstants = ns.corporation.getConstants()
	const researchNames: CorpResearchName[] = division.makesProducts ? constants.researchNamesProductOnly : constants.researchNamesBase
	const completedResearch: Map<CorpResearchName, boolean> = new Map()
	
	for (const researchName of researchNames) {
		completedResearch.set(researchName, ns.corporation.hasResearched(divisionName, researchName))
	}

	return Array.from(completedResearch.values()).every((value: boolean) => value === true)
}

async function getEmployeeRatios(ns: NS, divisionName: string, cityName: CityName) {
	const division: Division = ns.corporation.getDivision(divisionName)
	const isPostAI: boolean = ns.corporation.hasResearched(divisionName, 'Market-TA.II')
	const office: Office = ns.corporation.getOffice(divisionName, cityName)
	const completed: boolean = await hasAllResearchBeenCompleted(ns, divisionName, cityName)

	const main: Partial<Record<CorpEmployeePosition, number>> = {
		'Operations': office.size === 3 ? 1 : completed ? office.size * 0.26 : office.size * 0.25,
		'Engineer': office.size === 3 ? 1 : completed ? office.size * 0.26 : office.size * 0.25,
		'Business': office.size === 3 ? 1 : completed ? office.size * 0.26 : office.size * 0.25,
		'Management': office.size === 3 ? 0 : completed ? office.size * 0.1 : office.size * 0.05,
		'Research & Development': office.size === 3 ? 0 : completed ? 0 : office.size * 0.08,
		'Intern': office.size === 3 ? 0 : office.size * 0.12
	}

	const supporting: Partial<Record<CorpEmployeePosition, number>> = {
		'Operations': office.size === 3 ? 1 : office.size * 0.05,
		'Engineer': office.size === 3 ? 1 : office.size * 0.05,
		'Business': office.size === 3 ? 1 : office.size * 0.05,
		'Management': office.size === 3 ? 0 : office.size * 0,
		'Research & Development': office.size === 3 ? 0 : office.size * 0.73,
		'Intern': office.size === 3 ? 0 : office.size * 0.12
	}

	switch (division.type) {
		case 'Tobacco':
			switch (cityName) {
				case 'Aevum':
					return main
				default:
					return isPostAI ? main : supporting
			}
		default:
			return main
	}
}

async function assignEmployees(ns: NS): Promise<void> {
	const corp: CorporationInfo = ns.corporation.getCorporation()

	for (const divisionName of corp.divisions) {
		const division: Division = ns.corporation.getDivision(divisionName)
		const cityNames: CityName[] = division.cities

		for (const cityName of cityNames) {
			const office: Office = ns.corporation.getOffice(divisionName, cityName)
			const ratios: Partial<Record<CorpEmployeePosition, number>> = await getEmployeeRatios(ns, divisionName, cityName)

			let workersToBeEmployed: number = 0

			for (const ratio of Object.entries(ratios)) {
				const [_job, amount] = ratio
				const quantity: number = Math.floor(amount)
				workersToBeEmployed += quantity
			}

			for (const ratio of Object.entries(ratios)) {
				const [job, amount] = ratio
				let quantity: number = Math.floor(amount)

				const completed: boolean = await hasAllResearchBeenCompleted(ns, divisionName, cityName)
				const excessJob: string = completed ? 'Management' : 'Intern'

				if (job === excessJob) {
					quantity += office.size - workersToBeEmployed
				}

				ns.corporation.setAutoJobAssignment(divisionName, cityName, job, quantity)
			}
		}
	}
}

async function manageEmployees(ns: NS) {
	await hireEmployees(ns)
	await satisfyEmployees(ns)
	await unassignEmployees(ns)
	await assignEmployees(ns)
}

async function manageResearch(ns: NS) {
	const corp: CorporationInfo = ns.corporation.getCorporation()

	for (const divisionName of corp.divisions) {
		const division: Division = ns.corporation.getDivision(divisionName)

		let research: PrioritisedResearch[] = [
			{name: 'Hi-Tech R&D Laboratory', priority: 0, completed: false},
			{name: 'Market-TA.I', priority: 1, completed: false},
			{name: 'Market-TA.II', priority: 2, completed: false},
			{name: 'uPgrade: Fulcrum', priority: 3, completed: false, productIndustry: true},
			{name: 'uPgrade: Capacity.I', priority: 4, completed: false, productIndustry: true},
			{name: 'uPgrade: Capacity.II', priority: 5, completed: false, productIndustry: true},
			{name: 'Self-Correcting Assemblers', priority: 6, completed: false},
			{name: 'Overclock', priority: 7, completed: false},
			{name: 'Sti.mu', priority: 8, completed: false},
			{name: 'Drones', priority: 9, completed: false},
			{name: 'Drones - Assembly', priority: 10, completed: false},
			{name: 'Drones - Transport', priority: 11, completed: false},
			{name: 'Automatic Drug Administration', priority: 12, completed: false},
			{name: 'Go-Juice', priority: 13, completed: false},
			{name: 'CPH4 Injections', priority: 14, completed: false},
			{name: 'HRBuddy-Recruitment', priority: 100, completed: false},
			{name: 'HRBuddy-Training', priority: 100, completed: false},
			{name: 'uPgrade: Dashboard', priority: 100, completed: false, productIndustry: true},
			{name: 'AutoBrew', priority: 100, completed: false},
			{name: 'AutoPartyManager', priority: 100, completed: false},
		].sort((a: PrioritisedResearch, b: PrioritisedResearch) => a.priority - b.priority)

		let candidate: PrioritisedResearch | null = null

		research.forEach((research: PrioritisedResearch) => {
			research.completed = ns.corporation.hasResearched(divisionName, research.name)
		})

		for (let i = 0; i < research.length; i ++) {
			const specimen: PrioritisedResearch = research[i]

			if (!division.makesProducts && specimen.productIndustry) continue

			if (!specimen.completed) {
				candidate = specimen
				break
			}
		}

		if (!candidate) return

		const researchPoints: number = division.researchPoints
		const cost: number = ns.corporation.getResearchCost(divisionName, candidate.name)

		if (researchPoints > cost) {
			ns.corporation.research(divisionName, candidate.name)
			notify(ns, ['script', 'toast'], 'success', `Grogcorp: ${divisionName} -> Researched: ${candidate.name}`)
		}
	}
}

async function manageUpgrades(ns: NS, timesToExecute: number = 1) {
	for (let i = 0; i < timesToExecute; i ++) {
		const corp: CorporationInfo = ns.corporation.getCorporation()

		const upgradeNames: CorpUpgradeName[] = [
			'Smart Factories',
			'Smart Storage',
			'DreamSense',
			'Wilson Analytics',
			'Nuoptimal Nootropic Injector Implants',
			'Speech Processor Implants',
			'Neural Accelerators',
			'FocusWires',
			'ABC SalesBots',
			'Project Insight'
		]

		let candidate: {name: string, cost: number} | null = null

		for (const upgradeName of upgradeNames) {
			const cost: number = ns.corporation.getUpgradeLevelCost(upgradeName)

			if (candidate && candidate.cost > cost || !candidate) {
				candidate = {name: upgradeName, cost: cost}
			}
		}

		if (!candidate) return

		if (corp.funds * 0.05 > candidate.cost) {
			ns.corporation.levelUpgrade(candidate.name)
			notify(ns, 'script', 'success', `Grogcorp: Upgrade -> ${candidate.name} for ${ns.formatNumber(candidate.cost, 0)}`)
		}
	}
}

async function manageSales(ns: NS) {
	const corp: CorporationInfo = ns.corporation.getCorporation()

	for (const divisionName of corp.divisions) {
		const division: Division = ns.corporation.getDivision(divisionName)
		const industry: CorpIndustryData = ns.corporation.getIndustryData(division.type)
		const cityNames: CityName[] = division.cities
		const producedMaterials: CorpMaterialName[] | undefined = industry.producedMaterials

		if (producedMaterials) {
			for (const cityName of cityNames) {
				if (!ns.corporation.hasWarehouse(divisionName, cityName)) continue

				for (const producedMaterial of producedMaterials) {
					if (ns.corporation.hasResearched(divisionName, 'Market-TA.I')) {
						ns.corporation.setMaterialMarketTA1(divisionName, cityName, producedMaterial, true)
					}
					
					if (ns.corporation.hasResearched(divisionName, 'Market-TA.II')) {
						ns.corporation.setMaterialMarketTA2(divisionName, cityName, producedMaterial, true)
					}

					ns.corporation.sellMaterial(divisionName, cityName, producedMaterial, 'MAX', 'MP')
				}
			}
		}

		for (const product of division.products) {
			if (ns.corporation.hasResearched(divisionName, 'Market-TA.I')) {
				ns.corporation.setProductMarketTA1(divisionName, product, true)
			}

			if (ns.corporation.hasResearched(divisionName, 'Market-TA.II')) {
				ns.corporation.setProductMarketTA2(divisionName, product, true)
			}

			ns.corporation.sellProduct(divisionName, 'Sector-12', product, 'MAX', 'MP', true)
		}
	}
}

async function manageProducts(ns: NS) {
	const corp: CorporationInfo = ns.corporation.getCorporation()

	for (const divisionName of corp.divisions) {
		const division: Division = ns.corporation.getDivision(divisionName)
		const cityNames: CityName[] = division.cities

		if (!division.makesProducts) continue

		const productNames: string[] = division.products
		const capacity: number = division.maxProducts

		let idealCity: {name: CityName | null, employees: number} = {name: null, employees: 0}

		for (const cityName of cityNames) {
			const office: Office = ns.corporation.getOffice(divisionName, cityName)
			const employees: number = office.numEmployees

			if (employees > idealCity.employees) {
				idealCity = {name: cityName, employees: employees}
			}
		}

		if (!idealCity.name) continue

		const mostRecentProductName: string = productNames[productNames.length - 1]

		const weakestProductName: string = productNames.reduce((weakestProductName: string, currentProductName: string) => {
			const weakestProductRating: number = ns.corporation.getProduct(divisionName, idealCity.name!, weakestProductName).rating
			const currentProductRating: number = ns.corporation.getProduct(divisionName, idealCity.name!, currentProductName).rating

			return currentProductRating < weakestProductRating ? currentProductName : weakestProductName
		}, productNames[0])

		let nextProductName: string = 'Groguct.0'

		if (productNames.length > 0) {
			const split: string[] = mostRecentProductName.split('.')
			nextProductName = `${split[0]}.${Number(split[1]) + 1}`
		}

		if (productNames.length < capacity) {
			const totalFunding: number = corp.funds * 0.2
			ns.corporation.makeProduct(divisionName, idealCity.name, nextProductName, totalFunding / 2, totalFunding / 2)
			notify(ns, ['script', 'toast'], 'success', `Grogcorp: ${divisionName} -> ${idealCity.name} -> Create -> ${nextProductName} -> $${ns.formatNumber(totalFunding, 0)}`)
		} else {
			const product = ns.corporation.getProduct(divisionName, idealCity.name, mostRecentProductName)
			if (product.developmentProgress < 100) continue
			ns.corporation.discontinueProduct(divisionName, weakestProductName)
			notify(ns, ['script', 'toast'], 'success', `Grogcorp: ${divisionName} -> Discontinue -> ${weakestProductName}`)
		}
	}
}

async function manageWarehouse(ns: NS) {
	const corp: CorporationInfo = ns.corporation.getCorporation()

	if (shouldUpgradeWarehouseSize) {
		for (const divisionName of corp.divisions) {
			const division: Division = ns.corporation.getDivision(divisionName)
			const cityNames: CityName[] = division.cities

			for (const cityName of cityNames) {
				if (!ns.corporation.hasWarehouse(divisionName, cityName)) continue

				const warehouse: Warehouse = ns.corporation.getWarehouse(divisionName, cityName)
				const office: Office = ns.corporation.getOffice(divisionName, cityName)
				const totalProduction: number = (office.employeeProductionByJob as any).total
				const additionalSizeNeeded: number = ((warehouse.size * 0.4) - totalProduction) * -1
				const sizePerUpgrade: number = Math.ceil(warehouse.size / warehouse.level)
				const upgradesNeeded: number = Math.ceil(additionalSizeNeeded / sizePerUpgrade)

				ns.print(`additionalSizeNeeded: ${additionalSizeNeeded} -> sizePerUpgrade: ${sizePerUpgrade}`)

				if (totalProduction > 1e6) continue

				if (upgradesNeeded > 0) {
					const partialCost: number = ns.corporation.getUpgradeWarehouseCost(divisionName, cityName, 1)

					if (corp.funds > partialCost) {
						ns.corporation.upgradeWarehouse(divisionName, cityName, 1)
						notify(ns, ['script', 'toast'], 'success', `${divisionName} -> ${cityName} -> Warehouse -> +${ns.formatNumber(1 * sizePerUpgrade, 0)}`)
					}
				}
			}
		}
	}

	for (const divisionName of corp.divisions) {
		const division: Division = ns.corporation.getDivision(divisionName)
		const industryData: CorpIndustryData = ns.corporation.getIndustryData(division.type)
		const cityNames: CityName[] = division.cities

		const materials = new Map<CorpMaterialName, {impact: number, size: number, cost: number}>([
			['Hardware', {impact: industryData.hardwareFactor ?? 0, size: 0.06, cost: 0}],
			['Robots', {impact: industryData.robotFactor ?? 0, size: 0.5, cost: 0}],
			['AI Cores', {impact: industryData.aiCoreFactor ?? 0, size: 0.1, cost: 0}],
			['Real Estate', {impact: industryData.realEstateFactor ?? 0, size: 0.005, cost: 0}]
		])

		for (const key of materials.keys()) {
			for (const cityName of cityNames) {
				if (!ns.corporation.hasWarehouse(divisionName, cityName)) continue

				const typedKey: CorpMaterialName = key as CorpMaterialName
				const {impact, size} = materials.get(typedKey)!
				const material: Material = ns.corporation.getMaterial(divisionName, cityName, typedKey)
				materials.set(typedKey, {impact, size, cost: material.marketPrice})
			}
		}

		for (const cityName of cityNames) {
			if (!ns.corporation.hasWarehouse(divisionName, cityName)) continue

			const warehouse: Warehouse = ns.corporation.getWarehouse(divisionName, cityName)

			let capacity: number = 0

			switch (division.type) {
				case 'Agriculture':
					capacity = warehouse.size * 0.5
					break
				case 'Chemical':
					capacity = warehouse.size * 0.6
					break
				case 'Tobacco':
					capacity = warehouse.size * 0.6
					break
				case 'Water Utilities':
					capacity = warehouse.size * 0.6
					break
				default: 
					capacity = warehouse.size * 0.5
					break
			}

			let totalImpact: number = 0

			const weightings: Map<CorpMaterialName, number> = new Map<CorpMaterialName, number>()
			const sizes: Map<CorpMaterialName, number> = new Map<CorpMaterialName, number>()
			const quantities: Map<CorpMaterialName, number> = new Map<CorpMaterialName, number>()

			for (const {impact} of materials.values()) {
				totalImpact += impact
			}

			for (const [material, {impact}] of materials.entries()) {
				weightings.set(material, impact / totalImpact)
			}

			for (const [material, weight] of weightings.entries()) {
				sizes.set(material, weight * capacity)
			}

			for (const [material, allocatedSize] of sizes.entries()) {
				const {size} = materials.get(material)!
				quantities.set(material, Math.floor(allocatedSize / size))
			}

			for (const [materialName, quantity] of quantities.entries()) {
				const material: Material = ns.corporation.getMaterial(divisionName, cityName, materialName)
				if (division.type === 'Water Utilities' && material.name === 'Hardware') continue

				ns.corporation.sellMaterial(divisionName, cityName, materialName, '0', 'MP')

				if (material.stored > quantity) {					
					const difference: number = material.stored - quantity
					ns.corporation.sellMaterial(divisionName, cityName, materialName, (difference / 10).toString(), '0')
					notify(ns, 'script', 'success', `Grogcorp: ${divisionName} -> ${cityName} -> Sell -> ${materialName}: ${ns.formatNumber(difference, 0)}`)
				}

				if (material.stored < quantity) {
					if (warehouse.size - warehouse.sizeUsed < sizes.get(material.name)!) continue
					
					const corp: CorporationInfo = ns.corporation.getCorporation()
					const difference: number = quantity - material.stored

					if (corp.funds > (difference * material.marketPrice)) {
						ns.corporation.bulkPurchase(divisionName, cityName, materialName, difference)
						notify(ns, 'script', 'success', `Grogcorp: ${divisionName} -> ${cityName} -> Buy -> ${materialName}: ${ns.formatNumber(difference, 0)}`)
					}
				}
			}
		}
	}
}

export {
	manageEmployees,
	manageResearch,
	manageUpgrades,
	manageProducts,
	manageSales,
	manageWarehouse
}