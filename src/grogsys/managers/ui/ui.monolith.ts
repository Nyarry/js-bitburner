import {type NS} from '@ns'
import type {CorporationInfo, RunningScript} from '@ns'
import {initialise} from 'grogsys/shared/functions/initialise'

let startEpoch: number = 0

export async function main(ns: NS): Promise<void> {
	startEpoch = await initialise(ns, 'UI')

	await modifyCSS(ns)

	while (true) {
		await updateOverview(ns)
		await ns.sleep(1000)
	}
}

async function updateOverview(ns: NS) {
	const hook0: HTMLElement | null = document.getElementById('overview-extra-hook-0')
	const hook1: HTMLElement | null = document.getElementById('overview-extra-hook-1')

	if (!hook0 || !hook1) return

	let headers: string[] = []
	let values: string[] = []

	await getOverviewKarma(ns, headers, values)
	await getOverviewRatIncome(ns, headers, values)
	await getOverviewCorpIncome(ns, headers, values)

	hook0.innerText = headers.join(' \n')
	hook1.innerText = values.join(' \n')

	ns.atExit(() => {
		hook0.innerHTML = ''
		hook1.innerHTML = ''
	})
}

async function getOverviewKarma(ns: NS, headers: string[], values: string[]) {
	headers.push('Karma')
	values.push(`${ns.formatNumber(ns.heart.break())}`)
}

async function getOverviewRatIncome(ns: NS, headers: string[], values: string[]) {
	const runningScript: RunningScript | null = ns.getRunningScript('grogsys/managers/rats.js', 'home')

	if (!runningScript) return

	const income: number =  runningScript.onlineMoneyMade / runningScript.onlineRunningTime

	headers.push('Rats')
	values.push(`$${ns.formatNumber(income)} / s`)
}

async function getOverviewCorpIncome(ns: NS, headers: string[], values: string[]) {
	const corp: CorporationInfo = ns.corporation.getCorporation()

	headers.push('Corp')
	values.push(`$${ns.formatNumber(corp.revenue - corp.expenses, 3)} / s`)

	headers.push('Dividends')
	values.push(`$${ns.formatNumber(corp.dividendEarnings, 3)} / s`)
}

function loadGoogleFont(font: string) {
	const link: HTMLLinkElement = document.createElement('link')

	link.href = `https://fonts.googleapis.com/css?family=${font.split(' ').join('+')}:regular,semibold,bold,italic`
	link.rel = 'stylesheet'
	link.type = 'text/css'

	document.head.appendChild(link)
}

function replaceCSSProperty(property: any, oldValue: any, newValue: any) {
	const styleSheets: StyleSheetList = document.styleSheets

	for (let i = 0; i < styleSheets.length; i ++) {
		const sheet: CSSStyleSheet = styleSheets[i]
		const rules: CSSRuleList = sheet.cssRules

		for (let j = 0; j < rules.length; j ++) {
			const rule: CSSRule = rules[j]

			if (rule instanceof CSSStyleRule) {
				if (rule.style && rule.style[property] === oldValue) {
					rule.style[property] = newValue
				}
			}
		}
	}
}

function modifyCSSProperty(tagName: any, property: any, value: any) {
	const styleSheets: StyleSheetList = document.styleSheets

	for (let i = 0; i < styleSheets.length; i ++) {
		const sheet: CSSStyleSheet = styleSheets[i]
		const rules: CSSRuleList = sheet.cssRules

		for (let j = 0; j < rules.length; j ++) {
			const rule: CSSRule = rules[j]

			if (rule instanceof CSSStyleRule) {
				if (rule.selectorText === `${tagName}`) {
					rule.style[property] = value
					return
				}
			}
		}

		sheet.insertRule(`${tagName} {${property}: ${value};}`)
	}
}

function createStyleSheet() {
	const style: HTMLStyleElement = document.createElement('style')
	style.appendChild(document.createTextNode(''))
	document.head.appendChild(style)
	return style.sheet
}

function createRule(tagName: string, property: string, value: string | number) {
	return `
		${tagName} {
			${property}: ${value} !important;
		}
	`
}

async function modifyCSS(ns: NS) {
	const sheet: CSSStyleSheet | null = createStyleSheet()
	if (!sheet) return

	sheet.insertRule(createRule('.css-1cifzlv-primary', 'line-height', '1.2rem'))
	sheet.insertRule(createRule('.css-1qqe0gy-cell', 'padding-right', '2rem'))
	sheet.insertRule(createRule('.css-1qqe0gy-cell', 'border-bottom', 'none'))
	sheet.insertRule(createRule('.css-mztdf5-cell', 'border-bottom', 'none'))

	//const font: string = 'Source Sans Pro'

	//loadGoogleFont(font)
	//replaceCSSProperty('font-family', 'Consolas', font)
}