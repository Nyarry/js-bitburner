import {type NS} from '@ns'
import type {GangGenInfo, GangOtherInfo} from '@ns'
import type {Member, Rival} from 'grogsys/shared/types'
import {initialise} from 'grogsys/shared/functions/index'
import {getMembers} from 'grogsys/shared/aggregators/members'
import {AsciiTable} from 'grogsys/shared/thirdparty/ascii-table'

const table: AsciiTable = new AsciiTable('Gang', undefined)

export async function main(ns: NS): Promise<void> {
	await initialise(ns, 'Gang.Monolith')

	if (!ns.gang.inGang()) {
		const message: string = `Gang: You need to be in a gang. Teminating.`

		ns.toast(message, 'error', 10000)
		ns.tprint(`ERROR: ${message}`)
		ns.exit()
	}
	
	while (true) {
		await recruitMembers(ns)
		await manageGang(ns)
		await manageTable(ns)

		await ns.sleep(1000)
	}
}

async function manageGang(ns: NS) {
	const members: Member[] = await getMembers(ns)
	const atWar: boolean = await getWarDecision(ns)

	let membersToAscend: Member[] = []
	let membersToUpgrade: Member[] = []

	for (const member of members) {
		if (member.ascension) {
			membersToAscend.push(member)
		}

		if (member.buyableEquipment.length > 0) {
			membersToUpgrade.push(member)
		}
	}

	membersToUpgrade = membersToUpgrade.sort((a, b) => ns.gang.getEquipmentCost(b.buyableEquipment[0]) - ns.gang.getEquipmentCost(a.buyableEquipment[0]))

	for (const member of membersToAscend) {
		ascendMember(ns, member)
	}

	for (const member of membersToUpgrade) {
		upgradeMember(ns, member)
	}

	for (const member of members) {
		assignMember(ns, member)
	}

	ns.gang.setTerritoryWarfare(atWar)
}

async function recruitMembers(ns: NS) {
	if (!ns.gang.canRecruitMember()) return

	const names: string[] = [
		'Grogous', 'Groglan', 'Grogbim', 'Grogine', 'Grogire', 
		'Grogeis', 'Grogler', 'Grogera', 'Grogire', 'Grogbol', 
		'Grogani', 'Grogrim', 'Grogado', 'Grogner', 'Grogxic'
	]

	for (const name of names) {
		const wasRecruited: boolean = ns.gang.recruitMember(name)
		if (!wasRecruited) continue

		const message: string = `Gang: ${name} has joined the gang.`

		ns.toast(message, 'success', 10000)
		ns.print(`SUCCESS: ${message}`)
	}
}

async function ascendMember(ns: NS, member: Member): Promise<void> {
	if (!member.ascension) return

	const totalGain: number = member.ascension.str + member.ascension.def + member.ascension.dex + member.ascension.agi

	if (totalGain >= 6 && member.info.str >= 150) {
		ns.gang.ascendMember(member.name)

		const message: string = `Gang: ${member.name} has ascended.`
		ns.toast(message, 'success', 10000)
		ns.print(`SUCCESS: ${message}`)
	}
}

async function upgradeMember(ns: NS, member: Member): Promise<void> {
	const money: number = ns.getPlayer().money
	const equipmentToBuy: string = member.buyableEquipment[0]
	const costOfEquipment: number = ns.gang.getEquipmentCost(equipmentToBuy)

	if (money < costOfEquipment) return

	const wasPurchased: boolean = ns.gang.purchaseEquipment(member.name, equipmentToBuy)
	if (!wasPurchased) return

	const message: string = `Gang: ${member.name} has been equipped with [${equipmentToBuy}] for $${ns.formatNumber(costOfEquipment)}.`

	// ns.toast(message, 'success', 3000)
	ns.print(`SUCCESS: ${message}`)
}

async function getWarDecision(ns: NS): Promise<boolean> {
	const gang: GangGenInfo = ns.gang.getGangInformation()
	const otherGangs: GangOtherInfo = ns.gang.getOtherGangInformation()

	if (gang.territory === 1) return false

	let rivals: Rival[] = []

	for (const otherGang of Object.entries(otherGangs)) {
		const rival: Rival = {
			faction: otherGang[0], 
			power: otherGang[1].power, 
			territory: otherGang[1].territory, 
			clash: ns.gang.getChanceToWinClash(otherGang[0])
		}

		if (rival.faction === gang.faction) continue

		rivals.push(rival)
	}

	rivals = rivals.sort((a, b) => {
		return a.clash - b.clash
	})

	return rivals[0].clash > 0.75
}

async function assignMember(ns: NS, member: Member): Promise<void> {
	const gang: GangGenInfo = ns.gang.getGangInformation()

	let task: string = 'Human Trafficking'

	if (member.info.str < 5000 && member.info.earnedRespect >= 1e6) {
		task = 'Train Combat'
	}

	if (member.info.str >= 5000 && gang.respect < 100e6) {
		task = 'Terrorism'
	}

	if (member.info.str >= 500 && member.info.earnedRespect < 1e6) {
		task = 'Terrorism'
	}

	if (member.info.str >= 5000 && gang.territory < 100) {
		task = 'Territory Warfare'
	}

	if (member.info.str < 500) {
		task = 'Train Combat'
	}

	ns.gang.setMemberTask(member.name, task)
}

async function manageTable(ns: NS) {
	const columns: string[] = ['Member', 'str', 'def', 'dex', 'agi', 'total']
	table.setHeading(...columns)

	for (let i = 1; i <= columns.length; i ++) {
		table.setAlign(i, AsciiTable.RIGHT)
	}

	table.clearRows()

	const members: Member[] = await getMembers(ns)

	for (const member of members) {
		let str: number = 0
		let def: number = 0
		let dex: number = 0
		let agi: number = 0
		let total: number = 0
		
		if (member.ascension) {
			str = member.ascension.str
			def = member.ascension.def
			dex = member.ascension.dex
			agi = member.ascension.agi
			total = member.ascension.str + member.ascension.def + member.ascension.dex + member.ascension.agi
		}

		table.addRow(
			`${member.name}`,
			`${ns.formatNumber(str, 2)}`,
			`${ns.formatNumber(def, 2)}`,
			`${ns.formatNumber(dex, 2)}`,
			`${ns.formatNumber(agi, 2)}`,
			`${ns.formatNumber(total, 2)}`
		)
	}

	ns.print(table.toString())
}