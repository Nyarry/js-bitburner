import {type NS} from '@ns'
import {initialise} from 'grogsys/shared/functions/initialise'
import {AsciiTable} from 'grogsys/shared/thirdparty/ascii-table'
import {getPersonal} from 'grogsys/shared/aggregators/hosts'
import {clamp} from 'grogsys/shared/functions/clamp'
import {CONSTANTS} from 'grogsys/shared/constants/index'
import type {Machine, ProvisionType, ProvisionResponse} from 'grogsys/shared/types'

const table: AsciiTable = new AsciiTable('Personal Machines', undefined)

export async function main(ns: NS): Promise<void> {
	await initialise(ns, 'Provisioner')

	while (true) {
		await provisionMachines(ns)
		await manageTable(ns)
		await ns.sleep(2000)
	}
}

async function provisionMachines(ns: NS): Promise<void> {
	const limit: number = ns.getPurchasedServerLimit()

	let provisionResponses: ProvisionResponse[] = []

	for (let iteration = 0; iteration + 1 <= limit; iteration ++) {
		const hostName = `${CONSTANTS.NAMES.MACHINES.PERSONAL}.${iteration}`
		const provisionResponse: ProvisionResponse | undefined = await provisionMachine(ns, hostName)

		if (provisionResponse) {
			provisionResponses.push(provisionResponse)
		}
	}

	const purchases: ProvisionResponse[] = provisionResponses.filter((provisionResponse: ProvisionResponse) => provisionResponse.type === 'purchase')
	const upgrades: ProvisionResponse[] = provisionResponses.filter((provisionResponse: ProvisionResponse) => provisionResponse.type === 'purchase')

	if (purchases.length > 0) {
		const totalRAM: number = purchases.reduce((totalRAM: number, purchase: ProvisionResponse) => totalRAM + purchase.RAM, 0)
		const totalCost: number = purchases.reduce((totalCost: number, purchase: ProvisionResponse) => totalCost + purchase.cost, 0)
		const message: string = `Provisioner: Purchased ${purchases.length} machines with ${ns.formatRam(totalRAM)} RAM for $${ns.formatNumber(totalCost, 0)}`

		ns.toast(message, 'success', 10000)
		ns.print(message)
	}

	if (upgrades.length > 0) {
		const totalRAM: number = upgrades.reduce((totalRAM: number, purchase: ProvisionResponse) => totalRAM + purchase.RAM, 0)
		const totalCost: number = upgrades.reduce((totalCost: number, purchase: ProvisionResponse) => totalCost + purchase.cost, 0)
		const message: string = `Provisioner: Upgraded ${upgrades.length} machines with ${ns.formatRam(totalRAM)} RAM for $${ns.formatNumber(totalCost, 0)}`

		ns.toast(message, 'success', 10000)
		ns.print(message)
	}
}

async function provisionMachine(ns: NS, hostName: string): Promise<ProvisionResponse | undefined> {
	const purchasedServers: string[] = ns.getPurchasedServers()
	const limit: number = ns.getPurchasedServerLimit()
	
	if (purchasedServers.length < limit) {
		const baseRAM: number = 8
		const cost: number = ns.getPurchasedServerCost(baseRAM)
		const money: number = ns.getPlayer().money

		if (money > cost && !ns.serverExists(hostName)) {
			ns.purchaseServer(hostName, baseRAM)

			const provisionResponse: ProvisionResponse = {
				hostName,
				RAM: baseRAM,
				cost,
				type: 'purchase'
			}

			return provisionResponse
		}
	} else {
		const baseRAM: number = ns.getServerMaxRam(hostName)
		const upgradedRAM: number = baseRAM * 2
		const cost: number = ns.getPurchasedServerCost(upgradedRAM)
		const money: number = ns.getPlayer().money

		if (money > cost) {
			ns.killall(hostName)
			ns.deleteServer(hostName)
			ns.purchaseServer(hostName, upgradedRAM)

			const provisionResponse: ProvisionResponse = {
				hostName,
				RAM: upgradedRAM,
				cost,
				type: 'upgrade'
			}

			return provisionResponse
		}
	} 

	return undefined
}

async function manageTable(ns: NS) {
	const columns: string[] = ['hostName', 'RAM.used %', 'RAM.total']
	table.setHeading(...columns)

	for (let i = 1; i <= columns.length; i ++) {
		table.setAlign(i, AsciiTable.RIGHT)
	}

	table.clearRows()

	const personalMachines: Machine[] = await getPersonal(ns)

	for (const personalMachine of personalMachines) {
		const ratio: number = clamp(personalMachine.RAM.used / personalMachine.RAM.max, 0, 1)

		table.addRow(
			`${personalMachine.hostName}`,
			`${ns.formatPercent(ratio, 0)}`,
			`${ns.formatRam(personalMachine.RAM.max)}`
		)
	}

	ns.print(table.toString())
}