import {type NS} from '@ns'
import type {RunningScript} from '@ns'
import {initialise} from 'grogsys/shared/functions/initialise'
import {clamp} from 'grogsys/shared/functions/clamp'
import {getWorkers, getTargets} from 'grogsys/shared/aggregators/hosts'
import type {Worker, Target, RatMetric, ThreadType, ThreadState, State, Metrics} from 'grogsys/shared/types'
import {CONSTANTS} from 'grogsys/shared/constants/index'
import {AsciiTable} from 'grogsys/shared/thirdparty/ascii-table'

let startEpoch: number = 0
let state: State = {}
let metrics: Metrics = {}

const scripts: string[] = [CONSTANTS.SCRIPTS.WEAKEN, CONSTANTS.SCRIPTS.GROW, CONSTANTS.SCRIPTS.HACK]
const table: AsciiTable = new AsciiTable('Rats', undefined)

export async function main(ns: NS): Promise<void> {
	startEpoch = await initialise(ns, 'Rats')

	while (true) {
		const workers: Worker[] = await getWorkers(ns)
		const targets: Target[] = await getTargets(ns)

		await manageWorkers(ns, workers)
		await manageTargets(ns, targets, workers)
		await manageThreads(ns, targets, workers)
		await manageMetrics(ns)
		await manageTable(ns, targets)

		await ns.sleep(25)
	}
}

function getThreadTypeForScript(script: string): ThreadType | undefined {
	switch (script) {
		case CONSTANTS.SCRIPTS.WEAKEN:
			return 'w'
		case CONSTANTS.SCRIPTS.GROW:
			return 'g'
		case CONSTANTS.SCRIPTS.HACK:
			return 'h'
		default:
			return undefined
	}
}

async function manageWorkers(ns: NS, workers: Worker[]): Promise<void> {
	for (const worker of workers) {
		for (const script of scripts) {
			ns.scp(script, worker.hostName)
		}
	}
}

async function manageTargets(ns: NS, targets: Target[], workers: Worker[]): Promise<void> {
	for (const target of targets) {
		if (!state[target.hostName]) {
			state[target.hostName] = {
				cycle: 'w',
				w: {used: 0, needed: 0, total: 0},
				g: {used: 0, needed: 0, total: 0},
				h: {used: 0, needed: 0, total: 0}
			}
		}

		state[target.hostName] = {
			cycle: state[target.hostName].cycle,
			w: {used: 0, needed: 0, total: 0},
			g: {used: 0, needed: 0, total: 0},
			h: {used: 0, needed: 0, total: 0}
		}

		for (const worker of workers) {
			for (const script of scripts) {
				const runningScript: RunningScript | null = ns.getRunningScript(script, worker.hostName, target.hostName)
				if (!runningScript) continue

				const key: ThreadType | undefined = getThreadTypeForScript(script)
				if (!key) continue

				state[target.hostName][key].used += runningScript.threads
			}
		}

		if (target.security.difference > 0) {
			state[target.hostName].cycle = 'w'
		}

		if (state[target.hostName].cycle === 'w') {
			if (target.security.difference > 0) {
				const threadsToWeaken: number = Math.ceil(target.security.difference / 0.05)
				const threadState: ThreadState = state[target.hostName].w
				threadState.total = threadsToWeaken
				threadState.needed = Math.max(threadsToWeaken - threadState.used, 0)
			} else {
				state[target.hostName].cycle = 'g'
			}
		}
		if (state[target.hostName].cycle === 'g') {
			if (target.money.ratio !== 1 && target.money.ratio > 0) {
				const threadsToGrow: number = Math.ceil(ns.growthAnalyze(target.hostName, 1 / target.money.ratio))
				const threadState: ThreadState = state[target.hostName].g
				threadState.total = threadsToGrow
				threadState.needed = Math.max(threadsToGrow - threadState.used, 0)
			} else {
				state[target.hostName].cycle = 'h'
			}
		}
		if (state[target.hostName].cycle === 'h') {
			if (target.security.difference <= 5 && target.money.ratio >= 0.95) {
				const threadsToHack: number = Math.ceil(ns.hackAnalyzeThreads(target.hostName, target.money.max * 0.5))
				const threadState: ThreadState = state[target.hostName].h
				threadState.total = threadsToHack
				threadState.needed = Math.max(threadsToHack - threadState.used, 0)
			} else {
				state[target.hostName].cycle = 'w'
			}
		}
	}
}

async function purgeExistingThreads(ns: NS, workers: Worker[], script: string) {
	for (const worker of workers) {
		const wasPurged: boolean = ns.scriptKill(script, worker.hostName)

		if (wasPurged) {
			ns.print(`Rats: ${worker.hostName} ${getThreadTypeForScript(script)} cycle purged.`)
		}
	}
}

async function manageThreads(ns: NS, targets: Target[], workers: Worker[]): Promise<void> {
	for (const target of targets) {
		for (const worker of workers) {
			for (const script of scripts) {
				const key: ThreadType | undefined = getThreadTypeForScript(script)
				if (!key) continue

				const threadState: ThreadState = state[target.hostName][key]
				const numberOfPotentialThreads: number = Math.floor((ns.getServerMaxRam(worker.hostName) - ns.getServerUsedRam(worker.hostName)) / ns.getScriptRam(script))
				const numberOfThreadsToAllocate: number = Math.min(threadState.needed, numberOfPotentialThreads)

				if (threadState.used > threadState.total) {
					purgeExistingThreads(ns, workers, script)
				}

				if (numberOfThreadsToAllocate > 0) {
					threadState.used = Math.ceil(threadState.used + numberOfThreadsToAllocate)
					threadState.needed = threadState.total - threadState.used
					ns.exec(script, worker.hostName, numberOfThreadsToAllocate, target.hostName)
				}
			}
		}
	}
}

function isRatMetric(object: any): object is RatMetric {
	return (
		typeof object === 'object'
		&& object !== null
		&& typeof object.hostName === 'string' 
		&& typeof object.money === 'object'
		&& object.money !== null
		&& typeof object.money.stolen === 'number'
	)
}

async function manageMetrics(ns: NS) {
	if (ns.peek(1) !== 'NULL PORT DATA') {
		const data: any = JSON.parse(ns.readPort(1))

		if (isRatMetric(data)) {
			if (!metrics[data.hostName]) {
				metrics[data.hostName] = data
			}

			metrics[data.hostName].money.stolen += data.money.stolen
		}
	}

	let moneyStolenTotal: number = 0

	for (const [hostName, metric] of Object.entries(metrics)) {
		moneyStolenTotal += metric.money.stolen
	}

	const moneyStolenPerSecond: number = moneyStolenTotal / ((Date.now() - startEpoch) / 1000)
	ns.tryWritePort(2, JSON.stringify(moneyStolenPerSecond))
}

async function manageTable(ns: NS, targets: Target[]) {
	const columns: string[] = ['Target', 'weighting', 'cycle', 's.difference', 't.weaken', 't.grow', 't.hack', 'RAM.used %', 'RAM.needed', 'RAM.total', '$.max', '$.sec', '$.total']
	table.setHeading(...columns)

	for (let i = 1; i <= columns.length; i ++) {
		table.setAlign(i, AsciiTable.RIGHT)
	}

	table.clearRows()

	const weakenScriptRAM = ns.getScriptRam(CONSTANTS.SCRIPTS.WEAKEN)
	const growScriptRAM = ns.getScriptRam(CONSTANTS.SCRIPTS.WEAKEN)
	const hackScriptRAM = ns.getScriptRam(CONSTANTS.SCRIPTS.HACK)

	targets = targets.sort((a: Target, b: Target) => a.money.max - b.money.max)

	for (const target of targets) {
		const w = state[target.hostName].w
		const g = state[target.hostName].g
		const h = state[target.hostName].h

		const weaken: ThreadState = {
			used: weakenScriptRAM * w.used,
			needed: weakenScriptRAM * w.needed,
			total: weakenScriptRAM * w.total
		}

		const grow: ThreadState = {
			used: growScriptRAM * g.used,
			needed: growScriptRAM * g.needed,
			total: growScriptRAM * g.total
		}

		const hack: ThreadState = {
			used: hackScriptRAM * h.used,
			needed: hackScriptRAM * h.needed,
			total: hackScriptRAM * h.total
		}

		const usedRAM: number = weaken.used + grow.used + hack.used
		const neededRAM: number = weaken.needed + grow.needed + hack.needed
		const totalRAM: number = weaken.total + grow.total + hack.total
		const ratio: number = clamp(usedRAM / totalRAM, 0, 1)

		const moneyStolenTotal: number = metrics[target.hostName] ? metrics[target.hostName].money.stolen : 0
		const moneyStolenPerSecond: number = moneyStolenTotal / ((Date.now() - startEpoch) / 1000)

		table.addRow(
			`${target.hostName}`,
			`${ns.formatNumber(target.weighting)}`,
			`${state[target.hostName].cycle}`,
			`${ns.formatNumber(Math.ceil(target.security.difference), 0)}`,

			//`${ns.formatNumber(w.used + g.used + h.used, 0)}`,
			//`${ns.formatNumber(w.needed + g.needed + h.needed, 0)}`,
			//`${ns.formatNumber(w.total + g.total + h.total, 0)} `,

			`${ns.formatNumber(w.used, 0)} / ${ns.formatNumber(w.total, 0)}`,
			`${ns.formatNumber(g.used, 0)} / ${ns.formatNumber(g.total, 0)}`,
			`${ns.formatNumber(h.used, 0)} / ${ns.formatNumber(h.total, 0)}`,

			`${ns.formatPercent(!Number.isNaN(ratio) ? ratio : 0, 0)}`,
			//`${ns.formatRam(usedRAM, 0)} ${ratio > 0 ? '(' + ns.formatPercent(ratio, 0) + ')' : ''}`,
			`${ns.formatRam(neededRAM, 0)}`,
			`${ns.formatRam(totalRAM, 0)}`,
			`${ns.formatNumber(target.money.max, 0)}`,
			`${ns.formatNumber(moneyStolenPerSecond, 0)}`,
			`${ns.formatNumber(moneyStolenTotal, 0)}`
		)

		//table.addRow(
		//	target.hostName,
		//	`${ns.formatNumber(w.used, 0)} / ${ns.formatNumber(w.total, 0)} (${ns.formatNumber(w.needed, 0)})`,
		//	`${ns.formatNumber(g.used, 0)} / ${ns.formatNumber(g.total, 0)} (${ns.formatNumber(g.needed, 0)})`,
		//	`${ns.formatNumber(h.used, 0)} / ${ns.formatNumber(h.total, 0)} (${ns.formatNumber(h.needed, 0)})`,
		//	`${ns.formatNumber(target.weighting, 0)}`,
		//	`${ns.formatNumber(target.money.available, 0)} / ${ns.formatNumber(target.money.max, 0)} (${ns.formatPercent(target.money.ratio, 0)})`
		//)
	}

	ns.print(table.toString())
}