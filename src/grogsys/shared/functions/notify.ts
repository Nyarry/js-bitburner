import {type NS} from '@ns'
import {type UserInterfaceTheme} from '@ns'

type NotifyLocation = 'terminal' | 'script' | 'toast'
type NotifyType = 'info' | 'warning' | 'error' | 'success'
type RGB = {r: number, g: number, b: number}

function extractRGBFromHex(ns: NS, hex: string): RGB | null {
	if (!/^#([A-Fa-f0-9]{6})$/.test(hex)) return null

	hex = hex.slice(1)

	const r: number = parseInt(hex.substring(0, 2), 16)
	const g: number = parseInt(hex.substring(2, 4), 16)
	const b: number = parseInt(hex.substring(4, 6), 16)

	return {r, g, b}
}

function getColourCodeByType(ns: NS, notifyType: NotifyType) {
	const theme: UserInterfaceTheme = ns.ui.getTheme()

	switch (notifyType) {
		case 'info':
			const info: RGB | null = extractRGBFromHex(ns, theme.info)
			return info ? `\u001b[38;2;${info.r};${info.g};${info.b}m` : ''
		case 'warning':
			const warning: RGB | null = extractRGBFromHex(ns, theme.warning)
			return warning ? `\u001b[38;2;${warning.r};${warning.g};${warning.b}m` : ''
			case 'error':
			const error: RGB | null = extractRGBFromHex(ns, theme.error)
			return error ? `\u001b[38;2;${error.r};${error.g};${error.b}m` : ''
		case 'success':
			const success: RGB | null = extractRGBFromHex(ns, theme.success)
			return success ? `\u001b[38;2;${success.r};${success.g};${success.b}m` : ''
	}
}

async function notifyToTerminal(ns: NS, notifyType: NotifyType, message: string) {
	ns.tprint(`${getColourCodeByType(ns, notifyType)}${message}`)
}

async function notifyToScript(ns: NS, notifyType: NotifyType, message: string) {
	ns.print(`${getColourCodeByType(ns, notifyType)}${message}`)
}

async function notifyToToast(ns: NS, notifyType: NotifyType, message: string) {
	ns.toast(`${message}`, notifyType, 5000)
}

async function notify(ns: NS, notifyLocations: NotifyLocation | NotifyLocation[], notifyType: NotifyType, message: string) {
	if (!Array.isArray(notifyLocations)) {
		notifyLocations = [notifyLocations]
	}
	
	for (const notifyLocation of notifyLocations) {
		switch (notifyLocation) {
			case 'terminal':
				notifyToTerminal(ns, notifyType, message)
				break
			case 'script':
				notifyToScript(ns, notifyType, message)
				break
			case 'toast':
				notifyToToast(ns, notifyType, message)
				break
			default:
				notifyToTerminal(ns, notifyType, message)
		}
	}
}

export {
	notifyToTerminal,
	notifyToScript,
	notifyToToast,
	notify
}