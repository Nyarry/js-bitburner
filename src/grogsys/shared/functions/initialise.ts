import {type NS} from '@ns'

export async function initialise(ns: NS, descriptor: string): Promise<number> {
	const startEpoch: number = Date.now()
	const startTimestamp: string = new Date(startEpoch).toLocaleTimeString()

	ns.disableLog('ALL')
	ns.print(`${descriptor}: Initialised at ${startTimestamp}.`)
	ns.tprint(`INFO: ${descriptor}: Initialised at ${startTimestamp}.`)

	return startEpoch
}