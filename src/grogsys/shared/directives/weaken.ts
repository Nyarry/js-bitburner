import {type NS, type ScriptArg} from '@ns'

export async function main(ns: NS) {
	const scriptArgs: ScriptArg[] = ns.args
	const target: any = scriptArgs[0]

	if (typeof target === 'string') {
		await ns.weaken(target)
	} else {
		ns.toast(`Directive: Weaken -> Must be called with a valid hostName. (${target} is not a string)`)
	}
}