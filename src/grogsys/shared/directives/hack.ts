import {type NS, type ScriptArg} from '@ns'
import {type RatMetric} from 'grogsys/shared/types'

export async function main(ns: NS) {
	const scriptArgs: ScriptArg[] = ns.args
	const target: any = scriptArgs[0]

	if (typeof target === 'string') {
		const stolenMoney = await ns.hack(target)

		const ratMetric: RatMetric = {
			hostName: target,
			money: {
				stolen: stolenMoney
			}
		}

		ns.tryWritePort(1, JSON.stringify(ratMetric))
	} else {
		ns.toast(`Directive: Hack -> Must be called with a valid hostName. (${target} is not a string)`)
	}
}