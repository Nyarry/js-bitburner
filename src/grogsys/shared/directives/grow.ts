import {type NS, type ScriptArg} from '@ns'

export async function main(ns: NS) {
	const scriptArgs: ScriptArg[] = ns.args
	const target: any = scriptArgs[0]

	if (typeof target === 'string') {
		await ns.grow(target)
	} else {
		ns.toast(`Directive: Grow -> Must be called with a valid hostName. (${target} is not a string)`)
	}
}