import {type NS} from '@ns'
import type {GangGenInfo, GangMemberInfo, GangMemberAscension} from '@ns'
import {type Member} from 'grogsys/shared/types'

async function getMembers(ns: NS) {
	let members: Member[] = []

	const gang: GangGenInfo = ns.gang.getGangInformation()
	const memberNames: string[] = ns.gang.getMemberNames()

	for (const memberName of memberNames) {
		const info: GangMemberInfo = ns.gang.getMemberInformation(memberName)
		const ascension: GangMemberAscension | undefined = ns.gang.getAscensionResult(memberName)
		const ownedEquipment: string[] = info.upgrades.concat(info.augmentations)
		const equipmentNames: string[] = ns.gang.getEquipmentNames()

		let buyableEquipment: string[] = []

		for (const equipmentName of equipmentNames) {
			const upgradesToSkipIfCombat: string[] = ['NUKE Rootkit', 'Soulstealer Rootkit', 'Demon Rootkit', 'Hmap Node', 'Jack the Ripper']
			const augmentsToSkipIfCombat: string[] = ['BitWire', 'DataJack', 'Neuralstimulator']
			const upgradesToSkipIfHacking: string[] = ['Weapons', 'Armor', 'Vehicles']
			const augmentsToSkipIfHacking: string[] = ['Bionic Arms', 'Bionic Legs', 'Bionic Spine', 'BrachiBlades', 'Nanofiber Weave', 'Synthetic Heart', 'Synfibril Muscle', 'Graphene Bone Lacings']

			if (gang.isHacking) {
				if (upgradesToSkipIfHacking.includes(equipmentName)) continue
				if (augmentsToSkipIfHacking.includes(equipmentName)) continue
			} else {
				if (upgradesToSkipIfCombat.includes(equipmentName)) continue
				if (augmentsToSkipIfCombat.includes(equipmentName)) continue
			}

			if (!ownedEquipment.includes(equipmentName)) {
				buyableEquipment.push(equipmentName)
			}
		}

		buyableEquipment = buyableEquipment.sort((a, b) => {
			return ns.gang.getEquipmentCost(a) - ns.gang.getEquipmentCost(b)
		})

		const member: Member = {
			name: memberName,
			info,
			ascension,
			buyableEquipment
		}

		members.push(member)
	}

	return members
}

export {
	getMembers
}