import {type NS, type NodeStats} from '@ns'
import type {Node, NodeCosts, NodeGains, NodeCallbacks} from 'grogsys/shared/types'

async function getNodes(ns: NS): Promise<Node[]> {
	let nodes: Node[] = []

	for (let index = 0; index < ns.hacknet.numNodes(); index ++) {
		const stats: NodeStats = ns.hacknet.getNodeStats(index)

		const costs: NodeCosts = {
			level: ns.hacknet.getLevelUpgradeCost(index, 1),
			RAM: ns.hacknet.getRamUpgradeCost(index, 1),
			core: ns.hacknet.getCoreUpgradeCost(index, 1),
			cache: ns.hacknet.getCacheUpgradeCost(index, 1)
		}

		const gains: NodeGains = {
			money: {
				level: getMoneyGainRate(stats.level + 1, stats.ram, stats.cores),
				RAM: getMoneyGainRate(stats.level, getNextRAMIncrement(stats.ram), stats.cores),
				core: getMoneyGainRate(stats.level, stats.ram, stats.cores + 1)
			},
			hashes: {
				level: getHashGainRate(stats.level + 1, stats.ram, stats.cores),
				RAM: getHashGainRate(stats.level, getNextRAMIncrement(stats.ram), stats.cores),
				core: getHashGainRate(stats.level, stats.ram, stats.cores + 1)
			}
		}

		const callbacks: NodeCallbacks = {
			level: ns.hacknet.upgradeLevel,
			RAM: ns.hacknet.upgradeRam,
			core: ns.hacknet.upgradeCore,
			cache: ns.hacknet.upgradeCache
		}

		const node: Node = {
			index,
			stats,
			costs,
			gains,
			callbacks
		}

		nodes.push(node)
	}

	return nodes
}
 
function getMoneyGainRate(level: number, RAM: number, cores: number): number {
	const levelMultiplier: number = level * 1.5
	const RAMMultiplier: number = Math.pow(1.035, RAM - 1)
	const coreMultiplier: number = (cores + 5) / 6
	return levelMultiplier * RAMMultiplier * coreMultiplier
}

function getHashGainRate(level: number, RAM: number, cores: number): number {
	const baseGain: number = 0.001 * level
	const RAMMultiplier: number = Math.pow(1.07, Math.log2(RAM))
	const coreMultiplier: number = 1 + (cores - 1) / 5
	return Number((baseGain * RAMMultiplier * coreMultiplier).toFixed(4))
}

function getNextRAMIncrement(RAM: number): number {
	const levelsToMax: number = Math.round(Math.log2(64 / RAM))
	const numberOfTimesUpgraded: number = Math.log2(64) - levelsToMax
	return Math.pow(2, numberOfTimesUpgraded + 1)
}

export {
	getNodes
}