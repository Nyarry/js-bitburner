import {type NS} from '@ns'
import {type Machine, type Worker, type Target, type Executable, type ExposablePortState} from 'grogsys/shared/types'
import {CONSTANTS} from 'grogsys/shared/constants/index'

let hostNames: string[] = []

async function ping(ns: NS, network: string[] = ns.scan()) {
	const hostNamesToIgnore: string[] = ['darkweb']

	for (const hostName of network) {
		if (hostNamesToIgnore.includes(hostName)) continue
		if (hostNames.includes(hostName)) continue

		hostNames.push(hostName)
		await ping(ns, ns.scan(hostName))
	}
}

async function exposeMachine(ns: NS, hostName: string): Promise<boolean> {
	if (!hostName) return false
	if (!ns.serverExists(hostName)) return false
	if (ns.hasRootAccess(hostName)) return true

	const executables: Executable[] = [
		{file: 'BruteSSH.exe', callback: ns.brutessh},
		{file: 'FTPCrack.exe', callback: ns.ftpcrack},
		{file: 'relaySMTP.exe', callback: ns.relaysmtp},
		{file: 'HTTPWorm.exe', callback: ns.httpworm},
		{file: 'SQLInject.exe', callback: ns.sqlinject}
	]

	const exposablePortState: ExposablePortState = {
		required: ns.getServerNumPortsRequired(hostName),
		opened: 0
	}

	for (const executable of executables) {
		if (ns.fileExists(executable.file)) {
			executable.callback(hostName)
			exposablePortState.opened ++
		}
	}

	if (exposablePortState.opened >= exposablePortState.required) {
		ns.nuke(hostName)
		return true
	}

	return false
}

async function getTargetWeighting(ns: NS, machine: Machine): Promise<number> {
	const gainPerHack: number = (machine.money.available * ns.hackAnalyze(machine.hostName)) * ns.hackAnalyzeChance(machine.hostName)
	const gainPerSecond: number = gainPerHack / ns.getHackTime(machine.hostName)

	let threadWeighting: number = 1

	threadWeighting += Math.ceil(machine.security.difference / 0.05)
	threadWeighting += Math.ceil(ns.growthAnalyze(machine.hostName, isFinite(1 / machine.money.ratio) ? 1 / machine.money.ratio : 1))

	return (gainPerSecond / threadWeighting) * 100
}

async function getMachines(ns: NS): Promise<Machine[]> {
	await ping(ns, ns.scan())
	
	let machines: Machine[] = []

	for (const hostName of hostNames) {
		const isMachineExposed = await exposeMachine(ns, hostName)
		if (!isMachineExposed) continue

		const machine: Machine = {
			hostName,
			server: ns.getServer(hostName),
			money: {
				max: ns.getServerMaxMoney(hostName),
				available: ns.getServerMoneyAvailable(hostName),
				ratio: ns.getServerMoneyAvailable(hostName) / ns.getServerMaxMoney(hostName)
			},
			security: {
				min: ns.getServerMinSecurityLevel(hostName),
				current: ns.getServerSecurityLevel(hostName),
				difference: ns.getServerSecurityLevel(hostName) - ns.getServerMinSecurityLevel(hostName)
			},
			RAM: {
				max: ns.getServerMaxRam(hostName),
				used: ns.getServerUsedRam(hostName),
				free: ns.getServerMaxRam(hostName) - ns.getServerUsedRam(hostName)
			}
		}
	
		machines.push(machine)
	}

	return machines
}

async function getWorkers(ns: NS): Promise<Worker[]> {
	const machines: Machine[] = await getMachines(ns)
	let workers: Worker[] = []

	for (const machine of machines) {
		if (machine.hostName.includes('hacknet-server')) continue

		const worker = {
			...machine
		}

		workers.push(worker)
	}

	return workers.sort((a: Worker, b: Worker) => b.RAM.max - a.RAM.max)
}

async function getTargets(ns: NS): Promise<Target[]> {
	const machines: Machine[] = await getMachines(ns)
	let targets: Target[] = []

	for (const machine of machines) {
		if (machine.money.max <= 0) continue
		if (ns.getHackingLevel() < ns.getServerRequiredHackingLevel(machine.hostName)) continue

		const target = {
			...machine,
			weighting: await getTargetWeighting(ns, machine)
		}

		targets.push(target)
	}

	return targets.sort((a: Target, b: Target) => b.weighting - a.weighting)
}

async function getPersonal(ns: NS): Promise<Machine[]> {
	const machines: Machine[] = await getMachines(ns)
	let personalMachines: Machine[] = []

	for (const machine of machines) {
		if (machine.hostName.includes(CONSTANTS.NAMES.MACHINES.PERSONAL)) {
			personalMachines.push(machine)
		}
	}

	return personalMachines
}

async function getHacknet(ns: NS): Promise<Machine[]> {
	const machines: Machine[] = await getMachines(ns)
	let hacknetNodes: Machine[] = []

	for (const machine of machines) {
		if (machine.hostName.includes(CONSTANTS.NAMES.MACHINES.HACKNET)) {
			hacknetNodes.push(machine)
		}
	}

	return hacknetNodes.sort((a: Machine, b: Machine) => b.RAM.max - a.RAM.max)
}

export {
	getMachines,
	getWorkers,
	getTargets,
	getPersonal,
	getHacknet
}