import {type Server, type NodeStats} from '@ns'
import type {GangMemberInfo, GangMemberAscension} from '@ns'

export type Machine = {
	hostName: string,
	server: Server,
	money: {
		max: number,
		available: number,
		ratio: number
	},
	security: {
		min: number,
		current: number,
		difference: number
	},
	RAM: {
		max: number,
		used: number,
		free: number
	}
}

export interface Worker extends Machine {

}

export interface Target extends Machine {
	weighting: number
}

export type Executable = {
	file: string,
	callback: (hostName: string) => void
}

export type ExposablePortState = {
	required: number,
	opened: number
}

export type Node = {
	index: number ,
	stats: NodeStats,
	costs: NodeCosts,
	gains: NodeGains,
	callbacks: NodeCallbacks
}

export type NodeProspect = {
	index: number,
	upgradeType: string,
	cost: number,
	gain: number,
	callback: (index: number, n?: number) => boolean
}

export type NodeState = {
	level: number,
	RAM: number,
	core: number,
	cache?: number
}

export type NodeCosts = Required<NodeState>

export type NodeGains = {
	money: NodeState,
	hashes: NodeState
}

export type NodeCallbacks = {
	level: (index: number, n?: number) => boolean,
	RAM: (index: number, n?: number) => boolean,
	core: (index: number, n?: number) => boolean,
	cache: (index: number, n?: number) => boolean
}

export type Member = {
	name: string,
	info: GangMemberInfo,
	ascension: GangMemberAscension | undefined,
	buyableEquipment: string[]
}

export type Rival = {
	faction: string,
	power: number,
	territory: number,
	clash: number
}

export type RatMetric = {
	hostName: string,
	money: {
		stolen: number
	}
}

export type ProvisionType = 'purchase' | 'upgrade'

export type ProvisionResponse = {
	hostName: string,
	RAM: number,
	cost: number,
	type: ProvisionType
}

export type ThreadType = 'w' | 'g' | 'h'

export type ThreadState = {
	used: number,
	needed: number,
	total: number
}

export type State = {
	[key: string]: {
		cycle: ThreadType,
		w: ThreadState, 
		g: ThreadState, 
		h: ThreadState
	}
}

export type Metrics = {
	[key: string]: {
		money: {
			stolen: number
		}
	}
}

export type PrioritisedResearch = {
	priority: number,
	name: string,
	completed: boolean,
	productIndustry?: boolean
}