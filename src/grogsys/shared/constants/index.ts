export const CONSTANTS = {
	SCRIPTS: {
		WEAKEN: 'grogsys/shared/directives/weaken.js',
		GROW: 'grogsys/shared/directives/grow.js',
		HACK: 'grogsys/shared/directives/hack.js'
	},
	NAMES: {
		MACHINES: {
			HACKNET: 'hacknet-server',
			PERSONAL: 'grogcorp'
		}
	}
}